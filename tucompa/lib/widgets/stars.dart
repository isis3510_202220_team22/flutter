import 'package:flutter/material.dart';

class Stars extends StatelessWidget {
  final double rating;
  const Stars(this.rating, {super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(7),
        ),
      ),
      child: Row(
        children: [
          Icon(rating > 0
              ? (rating > 0.5 ? Icons.star : Icons.star_half)
              : Icons.star_border),
          Icon(rating > 1
              ? (rating > 1.5 ? Icons.star : Icons.star_half)
              : Icons.star_border),
          Icon(rating > 2
              ? (rating > 2.5 ? Icons.star : Icons.star_half)
              : Icons.star_border),
          Icon(rating > 3
              ? (rating > 3.5 ? Icons.star : Icons.star_half)
              : Icons.star_border),
          Icon(rating > 4
              ? (rating > 4.5 ? Icons.star : Icons.star_half)
              : Icons.star_border),
          Text(rating == 0 ? 'Sin calificación' : (rating).toStringAsFixed(2))
        ],
      ),
    );
  }
}
