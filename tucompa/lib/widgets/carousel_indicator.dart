import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

class CarouselWithIndicator extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _CarouselWithIndicatorState();
  }
}

class _CarouselWithIndicatorState extends State<CarouselWithIndicator> {
  int _current = 0;
  final CarouselController _controller = CarouselController();

  final List<String> textList = [
    'Encontrar personas que te acompañen en tu viaje en Transmilenio.',
    'Compartir ubicación en tiempo real con tus contactos preferidos',
    'Reportar y ver situaciones anómalas',
    'Si tienes inconvenientes con un acompañante, puedest reportar comportamientos inadecuados',
  ];
  @override
  Widget build(BuildContext context) {
    final List<Widget> imageSliders = textList
        .map((item) => Container(
              margin: const EdgeInsets.symmetric(horizontal: 15.0),
              child: Center(
                child: Text(
                  item,
                  textAlign: TextAlign.center,
                  style: const TextStyle(fontSize: 18),
                ),
              ),
            ))
        .toList();
    return Scaffold(
      backgroundColor: const Color.fromRGBO(216, 229, 237, 1),
      body: Column(
        children: [
          CarouselSlider(
            items: imageSliders,
            carouselController: _controller,
            options: CarouselOptions(
                height: 100,
                viewportFraction: 0.9,
                autoPlay: true,
                enlargeCenterPage: false,
                aspectRatio: 3.0,
                onPageChanged: (index, reason) {
                  setState(() {
                    _current = index;
                  });
                }),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: textList.asMap().entries.map((entry) {
              return GestureDetector(
                onTap: () => _controller.animateToPage(entry.key),
                child: Container(
                  width: 5.0,
                  height: 5.0,
                  margin: const EdgeInsets.symmetric(horizontal: 4.0),
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.black
                          .withOpacity(_current == entry.key ? 0.9 : 0.4)),
                ),
              );
            }).toList(),
          ),
        ],
      ),
    );
  }
}
