import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tucompa/bloc/user/user_bloc.dart';

import '../bloc/auth/auth_bloc.dart';

class RequestsCard extends StatelessWidget {
  final String email;
  final bool pending;
  final String routeId;
  final bool owner;
  final String ownerEmail;
  final Function leftButton;
  final Function rightButton;

  RequestsCard(this.email, this.pending, this.routeId, this.owner,
      this.ownerEmail, this.leftButton, this.rightButton);

  @override
  Widget build(BuildContext context) {
    String userEmail = '';
    if (context.read<AuthBloc>().state is Authenticated) {
      userEmail = context.read<AuthBloc>().state.props[0].toString();
    }

    return Container(
      decoration: const BoxDecoration(
        color: Color.fromRGBO(216, 229, 237, 1),
        borderRadius: BorderRadius.all(
          Radius.circular(9),
        ),
      ),
      height: 125,
      width: MediaQuery.of(context).size.width * 0.75,
      child: StreamBuilder(
        key: ValueKey(DateTime.now()),
        stream: BlocProvider.of<UserBloc>(context).getUserInfo(email),
        builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.data == null || snapshot.data!.size == 0) {
            const CircleAvatar(
              radius: 100,
              child: CircularProgressIndicator(),
            );
          } else {
            return Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 10),
                  child: CircleAvatar(
                    radius: MediaQuery.of(context).orientation ==
                            Orientation.portrait
                        ? MediaQuery.of(context).size.width * 0.09
                        : MediaQuery.of(context).size.width * 0.05,
                    foregroundImage: CachedNetworkImageProvider(
                        snapshot.data!.docs[0].get('image'),
                        cacheKey: email),
                    backgroundImage: const AssetImage(
                        'assets/images/profile-placeholder.png'),
                  ),
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 10, top: 12),
                        child: Text(
                          (() {
                            if (userEmail == email) {
                              return '${snapshot.data!.docs[0].get('name')} (Tú)';
                            }
                            if (email == ownerEmail) {
                              return '${snapshot.data!.docs[0].get('name')} (Admin)';
                            } else {
                              return snapshot.data!.docs[0].get('name');
                            }
                          }()),
                          style: const TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Color.fromRGBO(3, 69, 96, 1),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 30, top: 7),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              (snapshot.data!.docs[0].get('totalRating') /
                                      snapshot.data!.docs[0]
                                          .get('quantityRating'))
                                  .toStringAsFixed(2),
                              style: const TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Color.fromRGBO(3, 69, 96, 1),
                              ),
                            ),
                            const Icon(
                              Icons.star,
                              color: Color.fromRGBO(3, 69, 96, 1),
                            ),
                          ],
                        ),
                      ),
                      userEmail != email
                          ? Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(
                                    left: 20,
                                    top: 5,
                                    right: 5,
                                  ),
                                  child: RawMaterialButton(
                                    constraints: BoxConstraints.tight(
                                      const Size(36, 36),
                                    ),
                                    shape: const CircleBorder(),
                                    fillColor: Colors.white,
                                    onPressed: () {
                                      leftButton(routeId, email);
                                    },
                                    child: Icon(
                                      pending == true
                                          ? Icons.check
                                          : Icons.message_outlined,
                                      color: const Color.fromRGBO(3, 69, 96, 1),
                                    ),
                                  ),
                                ),
                                owner
                                    ? Padding(
                                        padding: const EdgeInsets.only(top: 2),
                                        child: RawMaterialButton(
                                          constraints: BoxConstraints.tight(
                                            const Size(36, 36),
                                          ),
                                          shape: const CircleBorder(),
                                          fillColor: Colors.white,
                                          onPressed: () {
                                            rightButton(routeId, email);
                                          },
                                          child: Icon(
                                            pending == true
                                                ? Icons.close
                                                : Icons.delete_outline,
                                            color: const Color.fromRGBO(
                                                3, 69, 96, 1),
                                          ),
                                        ),
                                      )
                                    : Container(),
                              ],
                            )
                          : Container(),
                    ],
                  ),
                )
              ],
            );
          }
          return Container();
        },
      ),
    );
  }
}
