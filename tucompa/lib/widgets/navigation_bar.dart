import 'package:flutter/material.dart';

class NavigationBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      backgroundColor: const Color.fromRGBO(3, 69, 96, 1),
      selectedItemColor: Colors.white,
      unselectedItemColor: Colors.white,
      showSelectedLabels: false,
      showUnselectedLabels: false,
      items: const [
        BottomNavigationBarItem(icon: Icon(Icons.home_outlined), label: ''),
        BottomNavigationBarItem(icon: Icon(Icons.search_sharp), label: ''),
        BottomNavigationBarItem(icon: Icon(Icons.map_outlined), label: ''),
        BottomNavigationBarItem(
            icon: Icon(Icons.account_circle_outlined), label: ''),
      ],
    );
  }
}
