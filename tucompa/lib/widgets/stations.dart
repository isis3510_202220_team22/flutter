import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tucompa/bloc/station/station_bloc.dart';

class Stations extends StatefulWidget {
  final Function getOrigin;
  final Function getDestination;
  String origin;
  String destination;

  Stations(this.getOrigin, this.getDestination, this.origin, this.destination,
      {super.key});

  @override
  State<Stations> createState() => _StationsState();
}

class _StationsState extends State<Stations> {
  void _setOrigin(String o) {
    setState(() {
      widget.origin = o;
    });
  }

  void _setDestination(String d) {
    setState(() {
      widget.destination = d;
    });
  }

  @override
  Widget build(BuildContext context) {
    if (widget.origin != 'Selecione...') {
      widget.getOrigin(widget.origin);
    }
    if (widget.destination != 'Selecione...') {
      widget.getDestination(widget.destination);
    }

    return Container(
      height: MediaQuery.of(context).orientation == Orientation.portrait
          ? MediaQuery.of(context).size.height * 0.2
          : MediaQuery.of(context).size.height * 0.32,
      width: MediaQuery.of(context).orientation == Orientation.portrait
          ? MediaQuery.of(context).size.width * 0.85
          : MediaQuery.of(context).size.width * 0.43,
      padding: const EdgeInsets.all(10),
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(7),
        ),
        color: Color.fromRGBO(216, 229, 237, 1),
      ),
      child: Row(
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              const Text(
                'Estacion origen',
                style: TextStyle(
                  color: Color.fromRGBO(3, 69, 96, 1),
                  fontWeight: FontWeight.bold,
                ),
              ),
              Container(
                height:
                    MediaQuery.of(context).orientation == Orientation.portrait
                        ? MediaQuery.of(context).size.height * 0.06
                        : MediaQuery.of(context).size.height * 0.09,
                width:
                    MediaQuery.of(context).orientation == Orientation.portrait
                        ? MediaQuery.of(context).size.width * 0.78
                        : MediaQuery.of(context).size.width * 0.38,
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(
                    Radius.circular(10),
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(
                    left: 7,
                    right: 7,
                  ),
                  child: BlocBuilder<StationBloc, StationState>(
                    builder: (context, state) {
                      List<String> stations = [];
                      if (state is LoadedStations) {
                        stations = state.stations;
                      }
                      return DropdownButtonHideUnderline(
                        child: DropdownButton(
                          value: widget.origin,
                          isExpanded: true,
                          borderRadius: BorderRadius.circular(20),
                          items: stations.isEmpty
                              ? [
                                  DropdownMenuItem(
                                    value: widget.origin,
                                    child: Text(widget.origin),
                                  )
                                ]
                              : stations.map((items) {
                                  return DropdownMenuItem(
                                    value: items,
                                    child: Text(items),
                                  );
                                }).toList(),
                          onChanged: (value) {
                            _setOrigin(value.toString());
                            widget.getOrigin(value);
                          },
                        ),
                      );
                    },
                  ),
                ),
              ),
              const Text(
                'Estacion destino',
                style: TextStyle(
                  color: Color.fromRGBO(3, 69, 96, 1),
                  fontWeight: FontWeight.bold,
                ),
              ),
              Container(
                height:
                    MediaQuery.of(context).orientation == Orientation.portrait
                        ? MediaQuery.of(context).size.height * 0.06
                        : MediaQuery.of(context).size.height * 0.08,
                width:
                    MediaQuery.of(context).orientation == Orientation.portrait
                        ? MediaQuery.of(context).size.width * 0.78
                        : MediaQuery.of(context).size.width * 0.38,
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(
                    Radius.circular(10),
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(
                    left: 7,
                    right: 7,
                  ),
                  child: BlocBuilder<StationBloc, StationState>(
                    builder: (context, state) {
                      List<String> stations = [];
                      if (state is LoadedStations) {
                        stations = state.stations;
                      }
                      return DropdownButtonHideUnderline(
                        child: DropdownButton(
                          value: widget.destination,
                          isExpanded: true,
                          borderRadius: BorderRadius.circular(20),
                          items: stations.isEmpty
                              ? [
                                  DropdownMenuItem(
                                    value: widget.destination,
                                    child: Text(widget.destination),
                                  )
                                ]
                              : stations.map((items) {
                                  return DropdownMenuItem(
                                    value: items,
                                    child: Text(items),
                                  );
                                }).toList(),
                          onChanged: (value) {
                            _setDestination(value.toString());
                            widget.getDestination(value);
                          },
                        ),
                      );
                    },
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
