import 'package:flutter/material.dart';

class Hour extends StatefulWidget {
  final Function getTime;
  String hour;
  String minutes;
  bool am;
  Hour(this.getTime, this.hour, this.minutes, this.am);

  @override
  State<Hour> createState() => _HourState();
}

class _HourState extends State<Hour> {
  void _setAm(bool a) {
    setState(() {
      widget.am = a;
    });
  }

  void _setHour(String h) {
    setState(() {
      widget.hour = h;
    });
  }

  void _setMinute(String m) {
    setState(() {
      widget.minutes = m;
    });
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        showTimePicker(
          context: context,
          initialTime: TimeOfDay.now(),
        ).then((value) {
          if (value == null) {
            return;
          }
          String pickedHour = value.hour.toString();
          if (int.parse(pickedHour) > 12) {
            pickedHour = (int.parse(pickedHour) - 12).toString();
            _setAm(false);
          } else {
            _setAm(true);
          }
          _setHour(pickedHour);
          String pickedMinute = value.minute.toString();
          if (pickedMinute.length == 1) {
            pickedMinute = '0$pickedMinute';
          }
          _setMinute(pickedMinute);
          widget.getTime(value);
        });
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.3,
          ),
          Container(
            margin: const EdgeInsets.only(
              top: 15,
            ),
            padding: const EdgeInsets.only(
              top: 10,
              bottom: 10,
              left: 30,
              right: 30,
            ),
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.all(
                Radius.circular(5),
              ),
              color: Color.fromRGBO(216, 229, 237, 1),
            ),
            child: Text(
              widget.hour,
              style: const TextStyle(
                color: Color.fromRGBO(3, 69, 96, 1),
                fontSize: 50,
              ),
            ),
          ),
          Column(
            children: const <Widget>[
              Padding(
                padding: EdgeInsets.only(
                  left: 8,
                  top: 15,
                ),
                child: CircleAvatar(
                  backgroundColor: Colors.black,
                  maxRadius: 3,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                  left: 8,
                  top: 15,
                ),
                child: CircleAvatar(
                  backgroundColor: Colors.black,
                  maxRadius: 3,
                ),
              ),
            ],
          ),
          Container(
            margin: const EdgeInsets.only(
              left: 8,
              top: 15,
            ),
            padding: const EdgeInsets.only(
              top: 10,
              bottom: 10,
              left: 30,
              right: 30,
            ),
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.all(
                Radius.circular(5),
              ),
              color: Color.fromRGBO(217, 217, 217, 1),
            ),
            child: Text(
              widget.minutes,
              style: const TextStyle(
                color: Colors.black,
                fontSize: 50,
              ),
            ),
          ),
          Column(
            children: <Widget>[
              Container(
                margin: const EdgeInsets.only(
                  left: 8,
                  top: 15,
                ),
                padding: const EdgeInsets.only(
                  top: 10,
                  bottom: 10,
                  left: 10,
                  right: 10,
                ),
                decoration: BoxDecoration(
                  borderRadius: const BorderRadius.all(
                    Radius.circular(5),
                  ),
                  color: widget.am
                      ? const Color.fromRGBO(216, 229, 237, 1)
                      : const Color.fromRGBO(217, 217, 217, 1),
                ),
                child: Text(
                  'AM',
                  style: TextStyle(
                    color: widget.am
                        ? const Color.fromRGBO(3, 69, 96, 1)
                        : Colors.black,
                    fontSize: 15,
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(
                  left: 8,
                  top: 5,
                ),
                padding: const EdgeInsets.only(
                  top: 10,
                  bottom: 10,
                  left: 10,
                  right: 10,
                ),
                decoration: BoxDecoration(
                  borderRadius: const BorderRadius.all(
                    Radius.circular(5),
                  ),
                  color: !widget.am
                      ? const Color.fromRGBO(216, 229, 237, 1)
                      : const Color.fromRGBO(217, 217, 217, 1),
                ),
                child: Text(
                  'PM',
                  style: TextStyle(
                    color: !widget.am
                        ? const Color.fromRGBO(3, 69, 96, 1)
                        : Colors.black,
                    fontSize: 15,
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
