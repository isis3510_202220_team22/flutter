import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tucompa/bloc/user_rating/user_rating_bloc.dart' as user_rating;
import 'package:tucompa/models/route.dart';
import 'package:tucompa/models/user.dart';
import 'package:tucompa/views/navigation_view.dart';
import 'package:tucompa/widgets/stars.dart';

import '../bloc/route/route_bloc.dart';

class ResultCard extends StatefulWidget {
  final User user;
  final RouteModel route;
  final bool hasRoute;
  final bool isInRoute;
  final bool isInThisRoute;
  final String loggedUser;
  const ResultCard(this.user, this.route, this.hasRoute, this.isInRoute,
      this.isInThisRoute, this.loggedUser,
      {super.key});

  @override
  State<ResultCard> createState() => _ResultCardState();
}

class _ResultCardState extends State<ResultCard> {
  String _getButtonText(AsyncSnapshot<QuerySnapshot> snap, String email) {
    if (email == widget.loggedUser) {
      return "Ruta propia";
    }
    if (widget.hasRoute) {
      return "No disponible";
    }
    if (widget.isInRoute == true && widget.isInThisRoute == false) {
      return "No disponible";
    }
    return (snap.data == null || snap.data!.size == 0)
        ? 'Enviar solicitud'
        : ((snap.data != null &&
                snap.data!.size > 0 &&
                snap.data!.docs[0]['pending'])
            ? 'Esperando'
            : 'Ir');
  }

  void _buttonAction(String routeId, String userEmail, String buttonText) {
    if (buttonText == 'Enviar solicitud') {
      user_rating.UserRatingState sta =
          BlocProvider.of<user_rating.UserRatingBloc>(context).state;
      if (sta is user_rating.Initial) {
        BlocProvider.of<user_rating.UserRatingBloc>(context)
            .add(user_rating.AddUserToRoute(userEmail, routeId));
      }
    }
    if (buttonText == 'Ir') {
      BlocProvider.of<RouteBloc>(context).add(Initialize());
      Navigator.of(context).pushReplacementNamed(NavigationView.routeName);
    }
  }

  @override
  Widget build(BuildContext context) {
    double rating = widget.user.quantityRating == 0
        ? 0
        : widget.user.totalRating / widget.user.quantityRating;
    DateTime depDate = widget.route.departureDate;
    return Card(
      key: ValueKey(DateTime.now()),
      color: const Color.fromRGBO(216, 229, 237, 1),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 20),
            child: CircleAvatar(
              key: ValueKey(DateTime.now()),
              radius: 40,
              backgroundImage: CachedNetworkImageProvider(
                widget.user.image,
                cacheKey: widget.user.email,
              ),
            ),
          ),
          Column(
            children: [
              Text(
                key: ValueKey(DateTime.now()),
                widget.user.name,
                style: const TextStyle(fontSize: 20),
              ),
              Text(
                  '${depDate.hour > 12 ? (depDate.hour - 12).toString() : depDate.hour.toString()}:${depDate.minute >= 10 ? depDate.minute : '0${depDate.minute}'} ${depDate.hour > 11 ? 'PM' : 'AM'}'),
              Stars(
                rating,
                key: ValueKey(DateTime.now()),
              ),
              StreamBuilder(
                  key: ValueKey(DateTime.now()),
                  stream: BlocProvider.of<user_rating.UserRatingBloc>(context)
                      .getUserStream(widget.loggedUser, widget.route.id!),
                  builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
                    if (snapshot.data == null || snapshot.data!.size == 0) {
                      BlocProvider.of<user_rating.UserRatingBloc>(context)
                          .add(user_rating.UserDenied());
                    }
                    return BlocBuilder<user_rating.UserRatingBloc,
                        user_rating.UserRatingState>(
                      builder: (con, sta) => TextButton(
                        key: ValueKey(DateTime.now()),
                        onPressed: widget.hasRoute ||
                                widget.user.email == widget.loggedUser
                            ? null
                            : () => _buttonAction(
                                widget.route.id!,
                                widget.loggedUser,
                                _getButtonText(
                                    snapshot, widget.route.userEmail)),
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all(
                              const Color.fromRGBO(3, 69, 96, 1)),
                          shape: MaterialStateProperty.all(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20),
                            ),
                          ),
                          padding: MaterialStateProperty.all(EdgeInsets.zero),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 10),
                          child: Text(
                            _getButtonText(snapshot, widget.route.userEmail),
                            style: const TextStyle(
                              fontSize: 10,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    );
                  }),
            ],
          )
        ],
      ),
    );
  }
}
