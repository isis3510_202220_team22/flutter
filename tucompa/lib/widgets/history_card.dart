import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class HistoryCard extends StatelessWidget {
  final String date;
  final String origin;
  final String destination;
  final String owner;
  final String image;

  HistoryCard(this.date, this.origin, this.destination, this.owner, this.image,
      {super.key});
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        color: Color.fromRGBO(216, 229, 237, 1),
        borderRadius: BorderRadius.all(
          Radius.circular(9),
        ),
      ),
      height: 300,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Row(
            children: <Widget>[
              Container(
                width: 150,
                margin: const EdgeInsets.only(left: 20, top: 15),
                padding: const EdgeInsets.all(7),
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(20),
                  ),
                  color: Color.fromRGBO(3, 69, 96, 1),
                ),
                child: const Text(
                  'Fecha',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Color.fromRGBO(216, 229, 237, 1),
                    fontSize: 15,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 25, top: 15),
                child: Text(
                  date,
                  style: const TextStyle(
                    color: Color.fromRGBO(3, 69, 96, 1),
                    fontSize: 15,
                  ),
                ),
              )
            ],
          ),
          Row(
            children: <Widget>[
              Container(
                margin: const EdgeInsets.only(left: 20, top: 15),
                padding: const EdgeInsets.all(7),
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(20),
                  ),
                  color: Color.fromRGBO(3, 69, 96, 1),
                ),
                child: const Text(
                  'Estación de origen',
                  style: TextStyle(
                    color: Color.fromRGBO(216, 229, 237, 1),
                    fontSize: 15,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 25, top: 15),
                child: Text(
                  origin,
                  style: const TextStyle(
                    color: Color.fromRGBO(3, 69, 96, 1),
                    fontSize: 15,
                  ),
                ),
              )
            ],
          ),
          Row(
            children: <Widget>[
              Container(
                margin: const EdgeInsets.only(left: 20, top: 15),
                padding: const EdgeInsets.all(7),
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(20),
                  ),
                  color: Color.fromRGBO(3, 69, 96, 1),
                ),
                child: const Text(
                  'Estación de destino',
                  style: TextStyle(
                    color: Color.fromRGBO(216, 229, 237, 1),
                    fontSize: 15,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20, top: 15),
                child: Text(
                  destination,
                  style: const TextStyle(
                    color: Color.fromRGBO(3, 69, 96, 1),
                    fontSize: 15,
                  ),
                ),
              )
            ],
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: const [
              Padding(
                padding: EdgeInsets.only(
                  left: 25,
                  top: 25,
                ),
                child: Text(
                  'Admin:',
                  textAlign: TextAlign.start,
                  style: TextStyle(
                    color: Color.fromRGBO(3, 69, 96, 1),
                    fontSize: 15,
                  ),
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(
              top: 5,
            ),
            child: Container(
              decoration: const BoxDecoration(
                color: Color.fromRGBO(242, 244, 245, 1),
                borderRadius: BorderRadius.all(
                  Radius.circular(9),
                ),
              ),
              height: 90,
              width: MediaQuery.of(context).size.width * 0.7,
              child: Row(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 10),
                    child: CircleAvatar(
                      radius: MediaQuery.of(context).orientation ==
                              Orientation.portrait
                          ? MediaQuery.of(context).size.width * 0.09
                          : MediaQuery.of(context).size.width * 0.05,
                      foregroundImage: CachedNetworkImageProvider(
                        image,
                        cacheKey: owner,
                      ),
                      backgroundImage: const AssetImage(
                          'assets/images/profile-placeholder.png'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      left: 25,
                    ),
                    child: Text(
                      owner,
                      textAlign: TextAlign.left,
                      style: const TextStyle(
                        color: Color.fromRGBO(3, 69, 96, 1),
                        fontSize: 15,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
