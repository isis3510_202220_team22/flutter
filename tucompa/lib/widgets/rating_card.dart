import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tucompa/bloc/user/user_bloc.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import '../bloc/auth/auth_bloc.dart';

class RatingCard extends StatefulWidget {
  final String email;
  final String ownerEmail;
  Function saveRating;

  RatingCard(this.email, this.ownerEmail, this.saveRating);

  @override
  State<RatingCard> createState() => _RatingCardState();
}

class _RatingCardState extends State<RatingCard> {
  bool inadequate = false;

  @override
  Widget build(BuildContext context) {
    String userEmail = '';
    if (context.read<AuthBloc>().state is Authenticated) {
      userEmail = context.read<AuthBloc>().state.props[0].toString();
    }

    return Container(
      decoration: const BoxDecoration(
        color: Color.fromRGBO(216, 229, 237, 1),
        borderRadius: BorderRadius.all(
          Radius.circular(9),
        ),
      ),
      height: 125,
      width: MediaQuery.of(context).size.width * 0.75,
      child: StreamBuilder(
        key: ValueKey(DateTime.now()),
        stream: BlocProvider.of<UserBloc>(context).getUserInfo(widget.email),
        builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.data == null || snapshot.data!.size == 0) {
            const CircleAvatar(
              radius: 100,
              child: CircularProgressIndicator(),
            );
          } else {
            return Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 10),
                  child: CircleAvatar(
                    radius: MediaQuery.of(context).orientation ==
                            Orientation.portrait
                        ? MediaQuery.of(context).size.width * 0.09
                        : MediaQuery.of(context).size.width * 0.05,
                    foregroundImage: CachedNetworkImageProvider(
                        snapshot.data!.docs[0].get('image'),
                        cacheKey: widget.email),
                    backgroundImage: const AssetImage(
                        'assets/images/profile-placeholder.png'),
                  ),
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 10, top: 12),
                        child: Text(
                          (() {
                            if (userEmail == widget.email) {
                              return '${snapshot.data!.docs[0].get('name')} (Tú)';
                            }
                            if (widget.email == widget.ownerEmail) {
                              return '${snapshot.data!.docs[0].get('name')} (Admin)';
                            } else {
                              return snapshot.data!.docs[0].get('name');
                            }
                          }()),
                          style: const TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Color.fromRGBO(3, 69, 96, 1),
                          ),
                        ),
                      ),
                      RatingBar(
                        ratingWidget: RatingWidget(
                            empty: const Icon(
                              Icons.star_outline_outlined,
                              color: Color.fromRGBO(3, 69, 96, 1),
                            ),
                            full: const Icon(
                              Icons.star,
                              color: Color.fromRGBO(3, 69, 96, 1),
                            ),
                            half: const Icon(
                              Icons.star_half,
                              color: Color.fromRGBO(3, 69, 96, 1),
                            )),
                        glow: false,
                        allowHalfRating: true,
                        onRatingUpdate: (value) =>
                            widget.saveRating(value, widget.email),
                      ),
                    ],
                  ),
                )
              ],
            );
          }
          return Container();
        },
      ),
    );
  }
}
