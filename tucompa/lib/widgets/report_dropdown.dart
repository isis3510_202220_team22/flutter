import 'package:flutter/material.dart';

class ReportDropdown extends StatefulWidget {
  void Function(BuildContext,String) selectItem;
  List<String> dropdownItems;
  int selectedItem;
  ReportDropdown(this.dropdownItems, this.selectItem, {super.key, this.selectedItem=0});

  @override
  State<ReportDropdown> createState() => _ReportDropdownState();
}

class _ReportDropdownState extends State<ReportDropdown> {
  @override
  Widget build(BuildContext context) {
    return InputDecorator(
      decoration: InputDecoration(
        filled: true,
        fillColor: const Color.fromRGBO(242, 244, 245, 1),
        border: OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.circular(30),
        ),
        contentPadding: const EdgeInsets.symmetric(horizontal: 10),
      ),
      child: DropdownButtonHideUnderline(
        child: DropdownButton(
          isExpanded: true,
          borderRadius: BorderRadius.circular(20),
          value: widget.dropdownItems[widget.selectedItem],
          items: widget.dropdownItems
              .map((e) => DropdownMenuItem(
                    value: e,
                    child: Text(e),
                  ))
              .toList(),
          onChanged: (value) {
            setState(() {
              widget.selectedItem = widget.dropdownItems.indexOf(value as String);
            });
            widget.selectItem(context, value as String);
          },
        ),
      ),
    );
  }
}
