import 'package:flutter/material.dart';

class ReportButton extends StatelessWidget {
  void Function(BuildContext) onTap;
  Icon buttonIcon;
  final String text;

  ReportButton(this.onTap, this.text, this.buttonIcon, {super.key});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => onTap(context),
      child: InputDecorator(
        decoration: InputDecoration(
          filled: true,
          fillColor: const Color.fromRGBO(242, 244, 245, 1),
          border: OutlineInputBorder(
            borderSide: BorderSide.none,
            borderRadius: BorderRadius.circular(30),
          ),
          contentPadding: const EdgeInsets.symmetric(horizontal: 10),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              text,
              style: const TextStyle(fontSize: 16),
            ),
            buttonIcon,
          ],
        ),
      ),
    );
  }
}
