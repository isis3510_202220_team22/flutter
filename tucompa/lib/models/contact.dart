class Contact {
  final String name;
  final String phoneNumber;
  final String image;

  Contact(this.name, this.phoneNumber, this.image);

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'phoneNumber': phoneNumber,
      'image': image,
    };
  }

  factory Contact.fromJson(Map<String, dynamic> json) {
    return Contact(
      json['name'],
      json['phoneNumber'],
      json['image'],
    );
  }
}
