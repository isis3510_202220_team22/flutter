class User {
  final String email;
  final String name;
  final String image;
  final bool isInRoute;
  final double totalRating;
  final int quantityRating;
  final int streak;

  User(
    this.email,
    this.name,
    this.image,
    this.isInRoute,
    this.totalRating,
    this.quantityRating,
    this.streak,
  );

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      json["email"],
      json["name"],
      json["image"],
      json["isInRoute"],
      double.parse(json["totalRating"].toString()),
      json["quantityRating"],
      json["streak"],
    );
  }
}
