class UserRating {
  final String userEmail;
  final double rate;
  final bool pending;
  final bool inadequate;

  UserRating(this.userEmail, this.rate, this.pending, this.inadequate);

  factory UserRating.fromJson(Map<String, dynamic> json) {
    return UserRating(
      json["userEmail"],
      json["rate"],
      json["pending"],
      json["inadequate"],
    );
  }
}