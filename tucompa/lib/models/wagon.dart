class Wagon {
  final String id;
  final String stationName;
  final String description;

  Wagon(this.id, this.stationName, this.description);
}
