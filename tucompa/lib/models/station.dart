class Station {
  final double latitude;
  final double longitude;
  final String name;

  Station(this.latitude, this.longitude, this.name);

  @override
  String toString() {
    return name;
  }
}
