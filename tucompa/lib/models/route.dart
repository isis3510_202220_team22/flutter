class RouteModel {
  final String? id;
  final DateTime departureDate;
  final String originStationName;
  final String destinationStationName;
  final String userEmail;

  RouteModel(this.departureDate, this.originStationName,
      this.destinationStationName, this.userEmail,
      [this.id]);

  Map<String, dynamic> toJson(route) {
    return {
      'id': route.id,
      'date': route.departureDate.toIso8601String(),
      'origin': route.originStationName,
      'destination': route.destinationStationName,
      'email': route.userEmail,
    };
  }

  factory RouteModel.fromJson(Map<String, dynamic> json) {
    return RouteModel(
      DateTime.parse(json["date"]),
      json["origin"],
      json["destination"],
      json["email"],
      json["id"],
    );
  }
}
