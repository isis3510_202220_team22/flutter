class Report {
  final String type;
  final DateTime date;
  final String description;

  Report(this.type, this.date, this.description);
}
