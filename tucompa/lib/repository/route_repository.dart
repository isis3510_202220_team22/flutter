import 'dart:async';
import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:sembast/sembast.dart';
import 'package:tucompa/models/user_rating.dart';
import '../models/route.dart';

class RouteRepository {
  final FirebaseFirestore _firebaseFirestore;
  Database db;
  RouteRepository(
    this.db, {
    FirebaseFirestore? firebaseFirestore,
  }) : _firebaseFirestore = firebaseFirestore ?? FirebaseFirestore.instance;

  Future<void> addRoute(RouteModel route, String email) async {
    final collection = _firebaseFirestore.collection('route');

    if (route.destinationStationName == 'Seleccione...' ||
        route.originStationName == 'Selecione...' ||
        route.destinationStationName == '' ||
        route.originStationName == '') {
      throw Exception("No ha seleccionado las estaciones de origen o destino");
    }
    if (route.departureDate.isBefore(DateTime.now())) {
      throw Exception("Debe seleccionar una hora válida");
    }
    if (route.destinationStationName == route.originStationName) {
      throw Exception(
          "Las estaciones de origen y destino deben ser diferentes");
    }
    try {
      await collection.add({
        'departureDate': route.departureDate,
        'destinationStationName': route.destinationStationName,
        'originStationName': route.originStationName,
        'userEmail': route.userEmail,
        'isFinished': false,
      }).then((docRef) {
        final userCollection =
            _firebaseFirestore.collection('route/${docRef.id}/userRating/');
        try {
          userCollection.doc(email).set({
            'inadequate': false,
            'pending': false,
            'rate': 0,
            'userEmail': email,
          });
          _firebaseFirestore.collection('user/').doc(email).update({
            'currentRoute': docRef.id,
            'isInRoute': true,
          }).timeout(const Duration(seconds: 3));
        } on TimeoutException {
          throw Exception(
              "Hubo un error al publicar la ruta, revise la conexión a internet, al conectarse la ruta se publicará");
        } catch (e) {
          throw Exception("Hubo un error al agregar la ruta");
        }
      });
    } catch (e) {
      throw Exception(
          "Hubo un error al agregar la ruta, revise la información ingresada");
    }
  }

  Future<List<RouteModel>> fetchFilterRoutes(
      String origin, String destination, DateTime dateTime) async {
    if (origin != 'Seleccione...' &&
        origin != 'Selecione...' &&
        destination != '' &&
        destination != '') {
      if (!dateTime.isBefore(DateTime.now())) {
        try {
          final collection = await _firebaseFirestore
              .collection('route')
              .where('originStationName', isEqualTo: origin)
              .where('destinationStationName', isEqualTo: destination)
              .where('departureDate',
                  isLessThanOrEqualTo: Timestamp.fromDate(
                      dateTime.add(const Duration(hours: 1))))
              .where('departureDate',
                  isGreaterThanOrEqualTo: Timestamp.fromDate(dateTime))
              .orderBy('departureDate')
              .get()
              .timeout(const Duration(seconds: 3));
          List<RouteModel> routes = collection.docs
              .map((doc) => RouteModel(
                    (doc['departureDate'] as Timestamp).toDate(),
                    doc['originStationName'],
                    doc['destinationStationName'],
                    doc['userEmail'],
                    doc.id,
                  ))
              .toList();
          return routes;
        } on TimeoutException {
          throw Exception(
              "Hubo un error al buscar las rutas, revise la conexión a internet y deslice hacia abajo para reintentar");
        } catch (e) {
          throw Exception(
              "Hubo un error al agregar la ruta, revise la información ingresada");
        }
      } else {
        throw Exception("Debe seleccionar una hora válida");
      }
    } else {
      throw Exception("No ha seleccionado las estaciones de origen o destino");
    }
  }

  Future<void> addUserToRoute(String routeId, UserRating userRating) async {
    final collection =
        _firebaseFirestore.collection('route/$routeId/userRating/');
    try {
      await collection.doc(userRating.userEmail).set({
        'inadequate': userRating.inadequate,
        'pending': userRating.pending,
        'rate': 0,
        'userEmail': userRating.userEmail,
      });
    } catch (e) {
      throw Exception(
          "Hubo un error al enviar la solicitud de unirse al viaje, intente de nuevo");
    }
  }

  Future<Map<String, UserRating>> getUserRatingRoutes(
      List<String> routeIds, String userEmail) async {
    try {
      Map<String, UserRating> routeRatings = {};
      final collection = await _firebaseFirestore
          .collection('route')
          .where(FieldPath.documentId, whereIn: routeIds)
          .get();
      for (var i = 0; i < routeIds.length; i++) {
        _firebaseFirestore
            .collection('route/${routeIds[i]}/userRating')
            .where('userEmail', isEqualTo: userEmail);
        routeRatings[routeIds[i]] =
            UserRating.fromJson(collection.docs[i].data());
      }
      return routeRatings;
    } catch (e) {
      throw Exception(
          "Hubo un error al obtener la solicitud de unirse al viaje, intente de nuevo");
    }
  }

  Future<void> acceptUserRating(String routeId, String userEmail) async {
    final collection =
        _firebaseFirestore.collection('route/$routeId/userRating/');
    try {
      await collection.doc(userEmail).update({
        'pending': false,
      });
      _firebaseFirestore.collection('user/').doc(userEmail).update({
        'currentRoute': routeId,
        'isInRoute': true,
      });
    } catch (e) {
      throw Exception(
          "Hubo un error al enviar la solicitud de unirse al viaje, intente de nuevo");
    }
  }

  Future<void> exitRoute(String routeId, String email) async {
    try {
      await _firebaseFirestore.collection('user/').doc(email).update({
        'currentRoute': '',
        'isInRoute': false,
      });
    } catch (e) {
      throw Exception("Hubo un error al salir de la calificación");
    }
  }

  Future<void> deleteRoute(String routeId) async {
    List<String> users = [];
    try {
      await _firebaseFirestore
          .collection('route/$routeId/userRating')
          .get()
          .then((value) {
        for (int i = 0; i < value.docs.length; i++) {
          users.add(value.docs[i].get('userEmail'));
        }
        for (int i = 0; i < users.length; i++) {
          deleteUserRating(routeId, users[i]);
        }
        final ref2 = _firebaseFirestore.collection('route/').doc(routeId);
        ref2.delete();
      });
    } catch (e) {
      throw Exception(
          "Hubo un error al enviar la solicitud de unirse al viaje, intente de nuevo");
    }
  }

  Future<void> deleteUserRating(String routeId, String userEmail) async {
    final ref = _firebaseFirestore
        .collection('route/$routeId/userRating/')
        .doc(userEmail);
    try {
      await ref.delete();
      _firebaseFirestore.collection('user/').doc(userEmail).update({
        'currentRoute': '',
        'isInRoute': false,
      });
    } catch (e) {
      throw Exception(
          "Hubo un error al enviar la solicitud de unirse al viaje, intente de nuevo");
    }
  }

  void sendRatings(
      Map<dynamic, dynamic> userRatings, String routeId, String email) {
    try {
      userRatings.forEach(
        (key, value) {
          _firebaseFirestore.collection('user/').doc(key).get().then((doc) {
            _firebaseFirestore.collection('user/').doc(key).update({
              'quantityRating': doc.get('quantityRating') + 1,
              'totalRating': doc.get('totalRating') + value,
            });
          }).timeout(const Duration(seconds: 3));

          _firebaseFirestore
              .collection('route/$routeId/userRating/')
              .doc(key)
              .update({
            'rate': value,
          }).timeout(const Duration(seconds: 3));
        },
      );

      _firebaseFirestore.collection('route/').doc(routeId).get().then((value) {
        _firebaseFirestore.collection('user/').doc(email).update({
          'isRating': false,
          'lastRoute': value.get('departureDate'),
        }).timeout(const Duration(seconds: 3));
      });
    } on TimeoutException {
      throw Exception(
          "Hubo un error al enviar las calificaciones, revise la conexión a internet, al conectarse se enviarán");
    } catch (e) {
      throw Exception("Hubo un error al enviar las calificaciones");
    }
  }

  Future<List<String>> checkRoute(String routeId) async {
    String id = '';
    String hour = '';
    String origin = '';
    String destination = '';
    String email = '';
    String finished = '';

    try {
      await _firebaseFirestore.collection('route/').doc(routeId).get().then(
        (value) {
          id = routeId;
          hour = value.get('departureDate').toDate().toString();
          origin = value.get('originStationName');
          destination = value.get('destinationStationName');
          email = value.get('userEmail');
          finished = value.get('isFinished').toString();
        },
      );
    } catch (e) {
      throw Exception('Hubo un error al encontrar la ruta actual');
    }
    return [id, hour, origin, destination, email, finished];
  }

  Future<List<String>> finishRoute(String routeId) async {
    try {
      List<String> users = [];
      await _firebaseFirestore.collection('route/').doc(routeId).update({
        'isFinished': true,
      });

      await _firebaseFirestore
          .collection('route/$routeId/userRating')
          .get()
          .then((value) {
        for (int i = 0; i < value.docs.length; i++) {
          users.add(value.docs[i].get('userEmail'));
        }
        for (int i = 0; i < users.length; i++) {
          _firebaseFirestore
              .collection('user/')
              .doc(users[i])
              .get()
              .then((value) {
            if (value.get('currentRoute') != '') {
              _firebaseFirestore.collection('user/').doc(users[i]).update({
                'isRating': true,
              });
            }
          });
        }
      });
    } catch (e) {}

    return checkRoute(routeId);
  }

  Future<List<RouteModel>> fetchHistory(String userEmail) async {
    var store = StoreRef<String, String>.main();
    var routesDB = await store.record('routes').get(db);
    var expired = await store.record('expire_routes').get(db);
    if (expired != null) {
      final stored = DateTime.fromMillisecondsSinceEpoch(int.parse(expired));
      if (stored.isBefore(DateTime.now())) {
        routesDB = null;
      }
    }
    List storedRoutes = [];
    if (routesDB != null) {
      List<RouteModel> routes = [];
      storedRoutes = jsonDecode(routesDB);
      if (storedRoutes[0]['email'] != userEmail) {
        store.delete(db);
      }
      if (storedRoutes[0]['email'] == userEmail) {
        for (int i = 1; i < storedRoutes.length; i++) {
          routes.add(RouteModel.fromJson(storedRoutes[i]));
        }
        return await compute(sortRoutes, routes);
      }
    }
    List<RouteModel> routes = [];
    storedRoutes.add({'email': userEmail});
    try {
      final collection = await _firebaseFirestore
          .collection('route')
          .get()
          .timeout(const Duration(seconds: 3));

      for (int i = 0; i < collection.docs.length; i++) {
        if (collection.docs[i]['isFinished']) {
          final users = await _firebaseFirestore
              .collection('route/${collection.docs[i].id}/userRating')
              .get()
              .timeout(const Duration(seconds: 3));
          final user = users.docs.where((element) => element.id == userEmail);

          if (user.isNotEmpty) {
            RouteModel route = RouteModel(
              (collection.docs[i]['departureDate'] as Timestamp).toDate(),
              collection.docs[i]['originStationName'],
              collection.docs[i]['destinationStationName'],
              collection.docs[i]['userEmail'],
              collection.docs[i].id,
            );
            routes.add(route);
            storedRoutes.add(route.toJson(route));
          }
        }
      }
      await store.record('routes').put(db, jsonEncode(storedRoutes));
      await store.record('expire_routes').put(
          db,
          DateTime.now()
              .add(const Duration(seconds: 120))
              .millisecondsSinceEpoch
              .toString());
    } on TimeoutException {
      throw Exception(
          "Hubo un error al cargar el historial de viajes, revise la conexión a internet e intente de nuevo");
    } catch (e) {
      throw Exception("Hubo un error al cargar el historial de viajes");
    }

    return await compute(sortRoutes, routes);
  }

  List<RouteModel> sortRoutes(List<RouteModel> data) {
    data.sort((a, b) => b.departureDate.compareTo(a.departureDate));
    return data;
  }

  Stream<QuerySnapshot<Map<String, dynamic>>> getUserRatingStream(
      String routeId, String userEmail) {
    try {
      return _firebaseFirestore
          .collection('route/$routeId/userRating/')
          .where('userEmail', isEqualTo: userEmail)
          .snapshots();
    } catch (e) {
      throw Exception(
          "Hubo un error al obtener la solicitud de unirse al viaje, intente de nuevo");
    }
  }

  Stream<QuerySnapshot<Map<String, dynamic>>> getUserRatings(String routeId) {
    try {
      return _firebaseFirestore
          .collection('route/$routeId/userRating/')
          .snapshots();
    } catch (e) {
      throw Exception("Hubo un error al cargar las solicitudes pendientes");
    }
  }
}
