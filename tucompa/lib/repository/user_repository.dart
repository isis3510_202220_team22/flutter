import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:tucompa/models/user.dart';

class UserRepository {
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  CollectionReference users = FirebaseFirestore.instance.collection('user');

  Future<User> getUserInfo(String email) async {
    DocumentSnapshot user = await users.doc(email).get();
    return User.fromJson(user.data() as Map<String, dynamic>);
  }

  Stream<QuerySnapshot<Map<String, dynamic>>> getUserInfoStream(
      String userEmail) {
    try {
      return firestore
          .collection('user/')
          .where('email', isEqualTo: userEmail)
          .snapshots();
    } catch (e) {
      throw Exception(
          "Hubo un error al obtener la solicitud de unirse al viaje, intente de nuevo");
    }
  }
}
