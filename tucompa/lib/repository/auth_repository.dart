import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';

class AuthRepository {
  final _auth = FirebaseAuth.instance;

  Future<void> login(
    String email,
    String password,
  ) async {
    try {
      await _auth
          .signInWithEmailAndPassword(
            email: email,
            password: password,
          )
          .timeout(const Duration(seconds: 3));
    } on TimeoutException {
      throw Exception(
          "Hubo un error al iniciar sesión, revise la conexión a internet e intente de nuevo");
    } catch (err) {
      throw Exception(
          "Hubo un problema con sus credenciales. Revise su correo y contraseña.");
    }
  }

  Future<void> signOut() async {
    try {
      await _auth.signOut();
    } catch (err) {
      throw Exception(err);
    }
  }
}
