import 'dart:async';
import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:tucompa/models/report.dart';
import 'package:tucompa/models/station.dart';
import 'package:shared_preferences/shared_preferences.dart';

class StationRepository {
  final FirebaseFirestore _firebaseFirestore;

  StationRepository({
    FirebaseFirestore? firebaseFirestore,
  }) : _firebaseFirestore = firebaseFirestore ?? FirebaseFirestore.instance;

  Future<List<String>> getStations() async {
    final prefs = await SharedPreferences.getInstance();
    List<String>? stations = [];

    stations = prefs.getStringList('stations');

    if (stations?.isNotEmpty ?? false) {
      return stations!;
    }

    try {
      QuerySnapshot collection =
          await _firebaseFirestore.collection('station').get();

      stations = collection.docs
          .map(
            (doc) => Station(
              doc['latitude'],
              doc['longitude'],
              doc['name'],
            ).toString(),
          )
          .toList();
      stations.add('Seleccione...');

      await prefs.setStringList('stations', stations);
      return stations;
    } catch (e) {
      throw Exception("Hubo un error al cargar las estaciones disponibles");
    }
  }

  Future<String> getWagonDescription(String stationName, String wagonId) async {
    try {
      QuerySnapshot collection = await _firebaseFirestore
          .collection('station')
          .doc(stationName.replaceAll(' ', ''))
          .collection('wagons')
          .where('id', isEqualTo: wagonId)
          .get();
      String d = collection.docs[0]['description'];
      return d;
    } catch (e) {
      throw Exception("Hubo un error al cargar la informacion del QR");
    }
  }

  Future<List<Map<String, dynamic>>> dangerousStation(
      String origin, String destination) async {
    try {
      DateTime date = DateTime.now();
      QuerySnapshot collectionOrigin = await _firebaseFirestore
          .collection('station')
          .doc(origin.replaceAll(' ', ''))
          .collection('reports')
          .where('dateTime',
              isGreaterThanOrEqualTo: DateTime(date.year, date.month, date.day))
          .get();
      QuerySnapshot collectionDestination = await _firebaseFirestore
          .collection('station')
          .doc(destination.replaceAll(' ', ''))
          .collection('reports')
          .where('dateTime',
              isGreaterThanOrEqualTo: DateTime(date.year, date.month, date.day))
          .get();

      List<Map<String, dynamic>> report = [];
      if (collectionOrigin.docs.isNotEmpty) {
        report.add({'name': origin, 'quantity': collectionOrigin.docs.length});
      }
      if (collectionDestination.docs.isNotEmpty) {
        report.add({
          'name': destination,
          'quantity': collectionDestination.docs.length
        });
      }
      return report;
    } catch (e) {
      throw Exception(
          "Hubo un error consultando los reportes de las estaciones");
    }
  }

  Future<List<Station>> getAllStations() async {
    try {
      QuerySnapshot collection =
          await _firebaseFirestore.collection('station').get();
      List<Station> stations = collection.docs
          .map(
            (doc) => Station(
              doc['latitude'],
              doc['longitude'],
              doc['name'],
            ),
          )
          .toList();
      return stations;
    } catch (e) {
      throw Exception("Hubo un error al cargar las estaciones disponibles");
    }
  }

  Future<Map<String, String>> routeSuggestion(Map<String, dynamic> data) async {
    try {
      final stations = await getAllStations();
      data['stations'] = stations;
      final minStation = await compute(getClosestStation, data);
      data['minStation'] = minStation;
      final routes = await _firebaseFirestore
          .collection('route')
          .where('userEmail', isEqualTo: data['email'])
          .where('originStationName', isEqualTo: minStation)
          .get();
      if (routes.docs.isEmpty) {
        return {'origin': data['minStation']};
      }
      data['routes'] =
          routes.docs.map((e) => e['destinationStationName']).toList();
      return await compute(getMaxRoute, data);
    } catch (e) {
      return {'suggestion': 'None', 'error': e.toString()};
    }
  }

  String getClosestStation(Map<String, dynamic> data) {
    double? minDistance;
    String minStation = '';
    List stations = data['stations'];
    double lat;
    double long;
    double distance;
    for (int i = 0; i < stations.length; i++) {
      lat = stations[i].latitude;
      long = stations[i].longitude;
      distance = sqrt(
          pow(data['latitude'] - lat, 2) + pow(data['longitude'] - long, 2));
      if (minDistance == null || distance < minDistance) {
        minDistance = distance;
        minStation = stations[i].name;
      }
    }
    return minStation;
  }

  Map<String, String> getMaxRoute(Map<String, dynamic> data) {
    Map<String, int> maxRoutes = {};
    List routes = data['routes'];
    var dest;
    for (int i = 0; i < routes.length; i++) {
      dest = routes[i];
      if (maxRoutes.containsKey(dest)) {
        maxRoutes[dest] = maxRoutes[dest]! + 1;
      } else {
        maxRoutes[dest] = 1;
      }
    }
    var max = 0;
    var destination = '';
    List<String> mapKeys = maxRoutes.keys.toList();
    for (int i = 0; i < mapKeys.length; i++) {
      if (maxRoutes[mapKeys[i]]! > max) {
        max = maxRoutes[mapKeys[i]]!;
        destination = mapKeys[i];
      }
    }

    if (destination == '') {
      return {'suggestion': 'None'};
    }
    return {'origin': data['minStation'], 'destination': destination};
  }

  Future<void> addReport(Report report, String station) async {
    try {
      final collection =
          _firebaseFirestore.collection('station/$station/reports');
      await collection.add({
        'dateTime': report.date,
        'description': report.description,
        'type': report.type,
      }).timeout(const Duration(seconds: 3));
    } on TimeoutException {
      throw Exception(
          "Hubo un error al enviar el reporte, revise la conexión a internet e intente de nuevo");
    } catch (e) {
      throw Exception("Hubo un error al agregar el reporte");
    }
  }
}
