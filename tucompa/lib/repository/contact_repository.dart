import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tucompa/models/contact.dart';

class ContactRepository {
  Future<void> saveContact(Contact contact, String user) async {
    try {
      final prefs = await SharedPreferences.getInstance();
      List<String>? storeContacts = prefs.getStringList('$user-contacts');
      if (storeContacts == null) {
        await prefs.setStringList('$user-contacts', [jsonEncode(contact)]);
      } else {
        storeContacts.add(jsonEncode(contact));
        await prefs.setStringList('$user-contacts', storeContacts);
      }
    } catch (err) {
      throw Exception(err);
    }
  }

  Future<List<Contact>> getContacts(String user) async {
    final prefs = await SharedPreferences.getInstance();
    List<String>? storeContacts = prefs.getStringList('$user-contacts');
    if (storeContacts == null) {
      return [];
    }
    return await compute(orderContacts,storeContacts);
  }

  List<Contact> orderContacts(List<String> contacts) {
    List<Contact> parsedContacts =
        contacts.map((e) => Contact.fromJson(jsonDecode(e))).toList();
    parsedContacts.sort((a, b) => a.name.compareTo(b.name));
    return parsedContacts;
  }

  Future<List<Contact>> deleteContact(Contact contact, String user) async {
    final prefs = await SharedPreferences.getInstance();
    List<String>? storeContacts = prefs.getStringList('$user-contacts');
    if (storeContacts != null) {
      List<String> contacts = await compute(deleteContactFromList,{'contacts':storeContacts,'deleted':jsonEncode(contact)});
      await prefs.setStringList(
        '$user-contacts',
        contacts,
      );
      return await compute(orderContacts,contacts);
    }
    return [];
  }

  List<String> deleteContactFromList(Map<String,dynamic> data) {
    List<String> contacts = data['contacts'];
    Contact contact = Contact.fromJson(jsonDecode(data['deleted']));
    List<Contact> parsedContacts =
        contacts.map((e) => Contact.fromJson(jsonDecode(e))).toList();
    parsedContacts.removeWhere((e) {
        if (e.name == contact.name &&
            e.phoneNumber == contact.phoneNumber &&
            e.image == contact.image) {
          return true;
        }
        return false;
      });
    return parsedContacts.map((e) => jsonEncode(e)).toList();
  }
}
