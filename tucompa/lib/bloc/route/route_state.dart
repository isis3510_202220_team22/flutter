part of 'route_bloc.dart';

abstract class RouteState extends Equatable {
  @override
  List<Object> get props => [];
}

class RouteInicial extends RouteState {
  @override
  List<Object> get props => [];
}

class CurrentRoute extends RouteState {
  final List<String> routeData;

  CurrentRoute(this.routeData);

  @override
  List<Object> get props => [];
}

class RouteLoading extends RouteState {
  @override
  List<Object> get props => [];
}

class RouteAdded extends RouteState {
  @override
  List<Object> get props => [];
}

class RouteError extends RouteState {
  final String error;

  RouteError(this.error);

  @override
  List<Object> get props => [];
}

class RoutesFetch extends RouteState {
  final List<RouteModel> routes;

  RoutesFetch(this.routes);

  @override
  List<Object> get props => [];
}
