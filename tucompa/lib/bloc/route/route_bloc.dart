import 'package:bloc/bloc.dart';
import 'package:tucompa/models/route.dart';
import 'package:equatable/equatable.dart';
import 'package:tucompa/repository/route_repository.dart';
import 'package:tucompa/repository/station_repository.dart';

part 'route_event.dart';
part 'route_state.dart';

class RouteBloc extends Bloc<RouteEvent, RouteState> {
  final RouteRepository routeRepository;
  final StationRepository stationRepository;

  RouteBloc({required this.routeRepository, required this.stationRepository})
      : super(RouteInicial()) {
    on<AddRoute>(
      (event, emit) async {
        emit(RouteLoading());
        try {
          await routeRepository.addRoute(event.route, event.email);
          emit(RouteAdded());
          await Future.delayed(const Duration(milliseconds: 800));
        } catch (e) {
          emit(
            RouteError(
              e.toString().replaceAll('Exception: ', ''),
            ),
          );
        }
      },
    );
    on<FetchRoutes>(
      (event, emit) async {
        emit(RouteLoading());
        try {
          List<RouteModel> routes = await routeRepository.fetchFilterRoutes(
              event.origin, event.destination, event.dateTime);
          emit(RoutesFetch(routes));
        } catch (e) {
          emit(RouteError(
            e.toString().replaceAll('Exception: ', ''),
          ));
        }
      },
    );
    on<CheckRoute>(
      (event, emit) async {
        try {
          List<String> routeData = [];
          await routeRepository
              .checkRoute(event.routeId)
              .then((List<String> value) {
            routeData = value;
          });
          emit(CurrentRoute(routeData));
        } catch (e) {
          emit(RouteError(
            e.toString().replaceAll('Exception: ', ''),
          ));
        }
      },
    );
    on<ExitRoute>(
      (event, emit) async {
        routeRepository.exitRoute(event.routeId, event.userEmail);
        emit(RouteInicial());
      },
    );
    on<RateRoute>(
      (event, emit) async {
        List<String> data = await routeRepository.finishRoute(event.routeId);
        emit(RouteInicial());
        emit(CurrentRoute(data));
      },
    );
    on<DeleteRoute>(
      (event, emit) {
        routeRepository.deleteRoute(event.routeId);
        emit(RouteInicial());
      },
    );
    on<Initialize>(
      (event, emit) {
        emit(RouteInicial());
      },
    );
  }
}
