part of 'route_bloc.dart';

abstract class RouteEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class Initialize extends RouteEvent {}

class AddRoute extends RouteEvent {
  final RouteModel route;
  final String email;

  AddRoute(this.route, this.email);
}

class FetchRoutes extends RouteEvent {
  final String origin;
  final String destination;
  final DateTime dateTime;

  FetchRoutes(this.origin, this.destination, this.dateTime);
}

class ExitRoute extends RouteEvent {
  final String routeId;
  final String userEmail;

  ExitRoute(this.routeId, this.userEmail);
}

class RateRoute extends RouteEvent {
  final String routeId;

  RateRoute(this.routeId);
}

class DeleteRoute extends RouteEvent {
  final String routeId;

  DeleteRoute(this.routeId);
}

class CheckRoute extends RouteEvent {
  final String routeId;

  CheckRoute(this.routeId);
}
