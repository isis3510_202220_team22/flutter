import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:tucompa/models/contact.dart';
import 'package:flutter/material.dart';
import 'package:tucompa/repository/contact_repository.dart';

part 'contact_event.dart';
part 'contact_state.dart';

class ContactBloc extends Bloc<ContactEvent, ContactState> {
  final ContactRepository contactRepository;
  ContactBloc({required this.contactRepository}) : super(Initial()) {
    on<ContactsRequested>((event, emit) async {
      emit(Loading());
      try {
        List<Contact> contacts =
            await contactRepository.getContacts(event.user);
        emit(Loaded(contacts));
      } catch (e) {
        emit(ContactError(
          e.toString().replaceAll('Exception: ', ''),
        ));
      }
    });

    on<SaveContact>((event, emit) async {
      emit(Loading());
      try {
        await contactRepository.saveContact(event.contact, event.user);
        List<Contact> contacts =
            await contactRepository.getContacts(event.user);
        emit(Loaded(contacts));
      } catch (e) {
        emit(ContactError(
          e.toString().replaceAll('Exception: ', ''),
        ));
      }
    });

    on<ExitContacts>((event, emit) async {
      emit(Initial());
    });

    on<DeleteContact>((event, emit) async {
      emit(Loading());
      try {
        List<Contact> contacts =
            await contactRepository.deleteContact(event.contact, event.user);
        emit(Loaded(contacts));
      } catch (e) {
        emit(ContactError(
          e.toString().replaceAll('Exception: ', ''),
        ));
      }
    });
  }
}
