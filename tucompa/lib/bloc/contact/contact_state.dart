part of 'contact_bloc.dart';

@immutable
abstract class ContactState extends Equatable {}

class Loading extends ContactState {
  @override
  List<Object?> get props => [];
}

class Initial extends ContactState {
  @override
  List<Object?> get props => [];
}

class Loaded extends ContactState {
  final List<Contact> contacts;

  Loaded(this.contacts);
  @override
  List<Object?> get props => [contacts];
}

class ContactError extends ContactState {
  final String error;

  ContactError(this.error);
  @override
  List<Object?> get props => [error];
}
