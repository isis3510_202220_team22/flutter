part of 'contact_bloc.dart';

abstract class ContactEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class ContactsRequested extends ContactEvent {
  final String user;
  ContactsRequested(this.user);
}

class SaveContact extends ContactEvent {
  final Contact contact;
  final String user;

  SaveContact(this.contact, this.user);
}

class ExitContacts extends ContactEvent {}

class DeleteContact extends ContactEvent {
  final String user;
  final Contact contact;

  DeleteContact(this.contact, this.user);
}
