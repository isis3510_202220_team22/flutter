import 'package:equatable/equatable.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:tucompa/repository/station_repository.dart';

part 'station_event.dart';
part 'station_state.dart';

class StationBloc extends Bloc<StationEvent, StationState> {
  final StationRepository stationRepository;

  StationBloc({required this.stationRepository}) : super(StationInitial()) {
    on<GetStations>((event, emit) async {
      List<String> stations;
      try {
        stations = await stationRepository.getStations();

        emit(LoadedStations(stations));
      } catch (e) {
        emit(
          StationError(
            e.toString().replaceAll('Exception: ', ''),
          ),
        );
      }
    });
    on<InitializeStations>((event, emit) async {
      try {
        emit(StationInitial());
      } catch (e) {
        emit(
          StationError(
            e.toString().replaceAll('Exception: ', ''),
          ),
        );
      }
    });
    on<GetWagonInfo>(((event, emit) async {
      try {
        emit(LoadingWagonInfo());
        String info = await stationRepository.getWagonDescription(
            event.stationName, event.wagonId);
        emit(WagonInformation(info));
      } catch (e) {
        emit(
          StationError(
            e.toString().replaceAll('Exception: ', ''),
          ),
        );
      }
    }));
    on<GetSuggestion>(
      ((event, emit) async {
        try {
          Map<String, String> suggestion =
              await stationRepository.routeSuggestion({
            'email': event.userEmail,
            'latitude': event.latitude,
            'longitude': event.longitude
          });
          emit(Suggestion(suggestion));
        } catch (e) {
          print(e);
          print('There was an error');
        }
      }),
    );
    on<ExitSuggestion>((event, emit) {
      emit(LoadedStations(event.stations));
    });
  }
}
