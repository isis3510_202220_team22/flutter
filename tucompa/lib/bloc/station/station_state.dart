part of 'station_bloc.dart';

abstract class StationState extends Equatable {
  @override
  List<Object> get props => [];
}

class StationInitial extends StationState {
  @override
  List<String> get props => [];
}

class LoadedStations extends StationState {
  final List<String> stations;
  LoadedStations(this.stations);
  @override
  List<String> get props => [];
}

class LoadingWagonInfo extends StationState {
  @override
  List<String> get props => [];
}

class WagonInformation extends StationState {
  final String description;
  WagonInformation(this.description);
  @override
  List<String> get props => [];
}

class StationError extends StationState {
  final String error;

  StationError(this.error);

  @override
  List<Object> get props => [];
}

class Suggestion extends StationState {
  final Map<String, String> suggestion;

  Suggestion(this.suggestion);

  @override
  List<String> get props => [];
}

class DangerousStation extends StationState {
  final List<Map<String, dynamic>> reports;

  DangerousStation(this.reports);

  @override
  List<String> get props => [];
}
