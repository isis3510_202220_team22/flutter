part of 'station_bloc.dart';

abstract class StationEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class InitializeStations extends StationEvent {
  @override
  List<Object> get props => [];
}

class GetStations extends StationEvent {
  @override
  List<Object> get props => [];
}

class GetWagonInfo extends StationEvent {
  final String stationName;
  final String wagonId;

  GetWagonInfo(this.stationName, this.wagonId);
}

class ExitSuggestion extends StationEvent {
  final List<String> stations;

  ExitSuggestion(this.stations);
}

class GetSuggestion extends StationEvent {
  final String userEmail;
  final double latitude;
  final double longitude;

  GetSuggestion(this.userEmail, this.latitude, this.longitude);
  @override
  List<Object> get props => [];
}
