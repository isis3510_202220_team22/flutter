part of 'user_bloc.dart';

abstract class UserEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class FetchRequested extends UserEvent {
  final String email;

  FetchRequested(this.email);
}

class UserLogout extends UserEvent {}