import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:tucompa/repository/user_repository.dart';

import '../../models/user.dart';

part 'user_event.dart';
part 'user_state.dart';

class UserBloc extends HydratedBloc<UserEvent, UserState> {
  final UserRepository userRepository;
  UserBloc({required this.userRepository}) : super(Loading()) {
    on<FetchRequested>((event, emit) async {
      emit(Loading(event.email));
      User user = await userRepository.getUserInfo(event.email);
      emit(FetchUser(user.email, user.name, user.image, user.isInRoute,
          user.totalRating, user.quantityRating, user.streak));
    });
    on<UserLogout>((event, emit) async {
      emit(Loading());
    });
  }
  Stream<QuerySnapshot<Map<String, dynamic>>> getUserInfo(String email) {
    return userRepository.getUserInfoStream(email);
  }

  @override
  UserState? fromJson(Map<String, dynamic> json) {
    if (json.isEmpty) {
      return Loading();
    }
    final user = User.fromJson(json);

    return FetchUser(user.email, user.name, user.image, user.isInRoute,
        user.totalRating, user.quantityRating, user.streak);
  }

  @override
  Map<String, dynamic>? toJson(UserState state) {
    if (state is FetchUser) {
      return state.toJson();
    } else {
      return {};
    }
  }
}
