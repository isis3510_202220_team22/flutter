part of 'user_bloc.dart';

@immutable
abstract class UserState extends Equatable {}

class Loading extends UserState {
  final String? email;

  Loading([this.email]);

  @override
  List<Object?> get props => [];
}

class FetchUser extends UserState {
  final String email;
  final String name;
  final String image;
  final bool isInRoute;
  final double totalRating;
  final int quantityRating;
  final int streak;

  FetchUser(
    this.email,
    this.name,
    this.image,
    this.isInRoute,
    this.totalRating,
    this.quantityRating,
    this.streak,
  );

  @override
  List<Object?> get props => [];

  Map<String, dynamic> toJson() {
    return {
      'email': email,
      'name': name,
      'image': image,
      'isInRoute': isInRoute,
      'totalRating': totalRating,
      'quantityRating': quantityRating,
      'streak': streak
    };
  }
}
