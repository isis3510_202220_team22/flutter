part of 'network_bloc.dart';

@immutable
abstract class NetworkState extends Equatable {}

class NetworkInitial extends NetworkState {
  @override
  List<Object?> get props => [];
}

class ConnectedState extends NetworkState {
  @override
  List<Object?> get props => [];
}

class NotConnectedState extends NetworkState {
  @override
  List<Object?> get props => [];
}
