part of 'network_bloc.dart';

abstract class NetworkEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class ConnectedEvent extends NetworkEvent{}

class NotConnectedEvent extends NetworkEvent {}