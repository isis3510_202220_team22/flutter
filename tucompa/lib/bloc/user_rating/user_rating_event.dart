part of 'user_rating_bloc.dart';

abstract class UserRatingEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class AddUserToRoute extends UserRatingEvent {
  final String userEmail;
  final String routeId;

  AddUserToRoute(this.userEmail, this.routeId);
}

class AcceptUser extends UserRatingEvent {
  final String userEmail;
  final String routeId;

  AcceptUser(this.routeId, this.userEmail);
}

class DeleteUser extends UserRatingEvent {
  final String userEmail;
  final String routeId;

  DeleteUser(this.routeId, this.userEmail);
}

class UserDenied extends UserRatingEvent {}

class SendRatings extends UserRatingEvent {
  final Map<dynamic, dynamic> ratings;
  final String routeId;
  final String email;

  SendRatings(this.ratings, this.routeId, this.email);
}
