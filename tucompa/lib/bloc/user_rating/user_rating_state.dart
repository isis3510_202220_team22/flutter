part of 'user_rating_bloc.dart';

@immutable
abstract class UserRatingState extends Equatable {}

class Loading extends UserRatingState {
  final String userEmail;
  final String routeId;

  Loading(this.userEmail, this.routeId);

  @override
  List<Object?> get props => [];
}

class Initial extends UserRatingState {
  @override
  List<Object?> get props => [];
}

class UserRatingError extends UserRatingState {
  final String error;

  UserRatingError(this.error);
  @override
  List<String> get props => [];
}

class AddedUserToRoute extends UserRatingState {
  @override
  List<Object?> get props => [];
}

class AddError extends UserRatingState {
  final String error;

  AddError(this.error);

  @override
  List<Object> get props => [];
}
