import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

import '../../repository/route_repository.dart';

import 'package:tucompa/models/user_rating.dart';

part 'user_rating_event.dart';
part 'user_rating_state.dart';

class UserRatingBloc extends Bloc<UserRatingEvent, UserRatingState> {
  final RouteRepository routeRepository;
  UserRatingBloc({required this.routeRepository}) : super(Initial()) {
    on<AddUserToRoute>(
      (event, emit) async {
        emit(Loading(event.userEmail, event.routeId));
        UserRating userRating = UserRating(event.userEmail, 0, true, false);
        try {
          await routeRepository.addUserToRoute(event.routeId, userRating);
          emit(AddedUserToRoute());
          emit(Initial());
        } catch (e) {
          emit(AddError(e.toString()));
        }
      },
    );

    on<UserDenied>(
      (event, emit) {
        emit(Initial());
      },
    );
    on<AcceptUser>(
      (event, emit) async {
        try {
          await routeRepository.acceptUserRating(
              event.routeId, event.userEmail);
        } catch (e) {
          emit(AddError(e.toString()));
        }
      },
    );
    on<DeleteUser>(
      (event, emit) async {
        try {
          await routeRepository.deleteUserRating(
              event.routeId, event.userEmail);
        } catch (e) {
          emit(AddError(e.toString()));
        }
      },
    );
    on<SendRatings>(
      (event, emit) async {
        try {
          routeRepository.sendRatings(
              event.ratings, event.routeId, event.email);
        } catch (e) {
          emit(AddError(e.toString()));
        }
      },
    );
  }

  Stream<QuerySnapshot<Map<String, dynamic>>> getUserStream(
      String userEmail, String routeId) {
    return routeRepository.getUserRatingStream(routeId, userEmail);
  }

  Stream<QuerySnapshot<Map<String, dynamic>>> getUserRatings(String routeId) {
    return routeRepository.getUserRatings(routeId);
  }
}
