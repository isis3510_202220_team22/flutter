import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:tucompa/models/report.dart';
import 'package:tucompa/repository/station_repository.dart';

part 'report_event.dart';
part 'report_state.dart';

class ReportBloc extends HydratedBloc<ReportEvent, ReportState> {
  final StationRepository stationRepository;
  ReportBloc({required this.stationRepository}) : super(ReportInitial()) {
    on<AddReport>((event, emit) async {
      emit(ReportLoading(
        type: event.report.type,
        station: event.station,
      ));
      try {
        await stationRepository.addReport(event.report, event.station);
        emit(ReportAdded());
      } catch (e) {
        emit(ReportError(
          e.toString().replaceAll('Exception: ', ''),
          type: event.report.type,
          station: event.station,
        ));
        await Future.delayed(const Duration(seconds: 3));
        emit(ReportInitial(
            type: event.report.type,
            date: event.report.date,
            time: TimeOfDay.fromDateTime(event.report.date),
            description: event.report.description,
            station: event.station));
      }
    });
    on<SaveReport>(
      (event, emit) {
        emit(ReportLoading());
        emit(ReportInitial(
            type: event.type,
            date: event.date,
            time: event.time,
            description: event.description,
            station: event.station));
      },
    );
    on<ExitReport>(
      (event, emit) {
        emit(ReportLoading());
        emit(ReportInitial());
      },
    );
  }

  @override
  ReportState? fromJson(Map<String, dynamic> json) {
    if (json.isEmpty) {
      return ReportInitial();
    } else {
      return ReportInitial(
          type: json['type'],
          date: json['date'] != null ? DateTime.parse(json['date']) : null,
          time: json['time'] != null
              ? TimeOfDay(
                  hour: int.parse(json['time'].split(":")[0]),
                  minute: int.parse(json['time'].split(":")[1]))
              : null,
          description: json['description'],
          station: json['station']);
    }
  }

  @override
  Map<String, dynamic>? toJson(ReportState state) {
    if (state is ReportInitial) {
      return {
        'type': state.type,
        'date': state.date?.toIso8601String(),
        'time': state.time != null
            ? '${state.time?.hour}:${state.time?.minute}'
            : null,
        'description': state.description,
        'station': state.station?.toString(),
      };
    }
    return {};
  }
}
