part of 'report_bloc.dart';

abstract class ReportEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class AddReport extends ReportEvent {
  final Report report;
  final String station;

  AddReport(this.report, this.station);
}

class SaveReport extends ReportEvent {
  final String? type;
  final DateTime? date;
  final TimeOfDay? time;
  final String? description;
  final String? station;

  SaveReport({this.type, this.date, this.time, this.description,this.station});
}

class ExitReport extends ReportEvent {}
