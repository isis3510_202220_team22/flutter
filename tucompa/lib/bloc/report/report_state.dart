part of 'report_bloc.dart';

@immutable
abstract class ReportState extends Equatable {}

class ReportInitial extends ReportState {
  final String? type;
  final DateTime? date;
  final TimeOfDay? time;
  final String? description;
  final String? station;

  ReportInitial({this.type, this.date, this.time, this.description, this.station});

  @override
  List<Object?> get props => [];
}

class ReportLoading extends ReportState {
  final String? type;
  final String? station;

  ReportLoading({this.type, this.station});
  
  @override
  List<Object?> get props => [];
}

class ReportAdded extends ReportState {
  @override
  List<Object?> get props => [];
}

class ReportError extends ReportState {
  final String error;
  final String? type;
  final String? station;

  ReportError(this.error, {this.type, this.station});
  
  @override
  List<Object?> get props => [];
}
