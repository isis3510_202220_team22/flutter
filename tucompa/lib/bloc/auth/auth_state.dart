part of 'auth_bloc.dart';

@immutable
abstract class AuthState extends Equatable {}

class Loading extends AuthState {
  @override
  List<Object?> get props => [];
}

class Initial extends AuthState {
  @override
  List<Object?> get props => [];
}

class Authenticated extends AuthState {
  final String email;

  Authenticated(this.email);
  @override
  List<Object?> get props => [email];
}

class UnAuthenticated extends AuthState {
  @override
  List<Object?> get props => [];
}

class AuthError extends AuthState {
  final String error;
  final String email;
  final String password;

  AuthError(this.error, this.email, this.password);
  @override
  List<Object?> get props => [error];
}
