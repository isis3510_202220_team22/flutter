import 'package:equatable/equatable.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:tucompa/repository/auth_repository.dart';
import 'package:flutter/material.dart';

part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends HydratedBloc<AuthEvent, AuthState> {
  final AuthRepository authRepository;
  AuthBloc({required this.authRepository}) : super(UnAuthenticated()) {
    on<SignInRequested>((event, emit) async {
      emit(Loading());
      try {
        await authRepository.login(event.email, event.password);
        emit(Authenticated(event.email));
      } catch (e) {
        emit(AuthError(e.toString().replaceAll('Exception: ', ''), event.email,
            event.password));
        emit(Initial());
      }
    });

    on<SignOutRequested>((event, emit) async {
      emit(Loading());
      await authRepository.signOut();
      emit(UnAuthenticated());
    });

    on<StartLogin>((event, emit) async {
      emit(Initial());
    });
  }

  @override
  AuthState? fromJson(Map<String, dynamic> json) {
    if (json.containsKey('auth') || json.containsKey('error')) {
      return UnAuthenticated();
    } else if (json.containsKey('email')) {
      return Authenticated(json['email']);
    } else if (json.containsKey('init')) {
      return Initial();
    } else {
      return Loading();
    }
  }

  @override
  Map<String, dynamic>? toJson(AuthState state) {
    if (state is Authenticated) {
      return {'email': state.email};
    } else if (state is UnAuthenticated) {
      return {'auth': false};
    } else if (state is Initial) {
      return {'init': true};
    } else if (state is AuthError) {
      return {'error': true};
    } else {
      return {};
    }
  }
}
