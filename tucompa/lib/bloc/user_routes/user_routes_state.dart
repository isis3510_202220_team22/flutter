part of 'user_routes_bloc.dart';

@immutable
abstract class UserRoutesState extends Equatable {}

class UserRoutesLoading extends UserRoutesState {
  UserRoutesLoading();

  @override
  List<Object?> get props => [];
}

class FetchComplete extends UserRoutesState {
  final List<User> users;
  final List<RouteModel> routes;
  final String origin;
  final String destination;
  final DateTime dateTime;

  FetchComplete(
      this.users, this.routes, this.origin, this.destination, this.dateTime);

  @override
  List<Object?> get props => [];
}

class History extends UserRoutesState {
  final List<RouteModel> routes;

  History(this.routes);

  @override
  List<Object?> get props => [];
}

class UserRoutesError extends UserRoutesState {
  final String error;
  final String? origin;
  final String? destination;
  final DateTime? dateTime;

  UserRoutesError(this.error, [this.origin, this.destination, this.dateTime]);

  @override
  List<Object> get props => [];
}
