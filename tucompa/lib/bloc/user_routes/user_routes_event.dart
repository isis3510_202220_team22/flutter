part of 'user_routes_bloc.dart';

abstract class UserRoutesEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class FetchRoutes extends UserRoutesEvent {
  final String origin;
  final String destination;
  final DateTime dateTime;

  FetchRoutes(this.origin, this.destination, this.dateTime);
}

class FetchHistory extends UserRoutesEvent {
  final String userEmail;

  FetchHistory(this.userEmail);
}

class ExitView extends UserRoutesEvent {}
