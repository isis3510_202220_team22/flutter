import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:tucompa/models/route.dart';
import 'package:tucompa/models/user.dart';
import 'package:tucompa/repository/route_repository.dart';
import 'package:tucompa/repository/user_repository.dart';
part 'user_routes_event.dart';
part 'user_routes_state.dart';

class UserRoutesBloc extends Bloc<UserRoutesEvent, UserRoutesState> {
  final UserRepository userRepository;
  final RouteRepository routeRepository;
  UserRoutesBloc({required this.userRepository, required this.routeRepository})
      : super(UserRoutesLoading()) {
    on<FetchRoutes>(
      (event, emit) async {
        emit(UserRoutesLoading());
        try {
          List<RouteModel> routes = await routeRepository.fetchFilterRoutes(
              event.origin, event.destination, event.dateTime);
          List<User> users = [];
          User user;
          for (int i = 0; i < routes.length; i++) {
            user = await userRepository.getUserInfo(routes[i].userEmail);
            users.add(user);
          }
          emit(FetchComplete(
            users,
            routes,
            event.origin,
            event.destination,
            event.dateTime,
          ));
        } catch (e) {
          emit(UserRoutesError(
            e.toString().replaceAll('Exception: ', ''),
            event.origin,
            event.destination,
            event.dateTime,
          ));
        }
      },
    );

    on<FetchHistory>((event, emit) async {
      emit(UserRoutesLoading());

      try {
        List<RouteModel> routes =
            await routeRepository.fetchHistory(event.userEmail);
        emit(History(routes));
      } catch (e) {
        emit(UserRoutesError(e.toString().replaceAll('Exception: ', '')));
      }
    });

    on<ExitView>((event, emit) async {
      emit(UserRoutesLoading());
    });
  }
}
