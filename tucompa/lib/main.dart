import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:path_provider/path_provider.dart';
import 'package:tucompa/bloc/contact/contact_bloc.dart';
import 'package:tucompa/bloc/network/network_bloc.dart';
import 'package:tucompa/bloc/report/report_bloc.dart';
import 'package:tucompa/bloc/user_rating/user_rating_bloc.dart';
import 'package:tucompa/bloc/user_routes/user_routes_bloc.dart';
import 'package:tucompa/repository/contact_repository.dart';
import 'package:tucompa/repository/route_repository.dart';
import 'package:tucompa/repository/station_repository.dart';
import 'package:tucompa/views/history_view.dart';
import 'package:tucompa/views/add_contact_view.dart';
import 'package:tucompa/views/qr_view.dart';
import 'package:tucompa/bloc/auth/auth_bloc.dart';
import 'package:tucompa/bloc/user/user_bloc.dart';
import 'package:tucompa/repository/auth_repository.dart';
import 'package:tucompa/repository/user_repository.dart';
import 'package:tucompa/views/login_view.dart';
import 'package:tucompa/views/profile_view.dart';
import 'package:tucompa/views/navigation_view.dart';
import 'package:tucompa/views/reports_view.dart';
import 'package:tucompa/views/search_result_view.dart';
import 'package:path/path.dart';
import 'package:sembast/sembast_io.dart';
import 'package:tucompa/views/contacts_view.dart';
import 'bloc/route/route_bloc.dart';
import 'bloc/station/station_bloc.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  HydratedBloc.storage = await HydratedStorage.build(
      storageDirectory: await getApplicationSupportDirectory());
  var dir = await getApplicationDocumentsDirectory();

  await dir.create(recursive: true);
  var dbPath = join(dir.path, 'my_database.db');
  var db = await databaseFactoryIo.openDatabase(dbPath);

  runApp(
    MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => NetworkBloc(),
        ),
        BlocProvider(
          create: (context) => AuthBloc(authRepository: AuthRepository()),
        ),
        BlocProvider(
          create: (context) => UserBloc(userRepository: UserRepository()),
        ),
        BlocProvider(
          create: (context) =>
              StationBloc(stationRepository: StationRepository()),
        ),
        BlocProvider(
          create: (context) => UserRoutesBloc(
              userRepository: UserRepository(),
              routeRepository: RouteRepository(db)),
        ),
        BlocProvider(
          create: (context) =>
              UserRatingBloc(routeRepository: RouteRepository(db)),
        ),
        BlocProvider(
          create: (context) => RouteBloc(
            routeRepository: RouteRepository(db),
            stationRepository: StationRepository(),
          ),
        ),
        BlocProvider(
          create: (context) =>
              ReportBloc(stationRepository: StationRepository()),
        ),
        BlocProvider(
          create: (context) =>
              ContactBloc(contactRepository: ContactRepository()),
        ),
      ],
      child: const MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: BlocBuilder<AuthBloc, AuthState>(
        builder: (context, state) {
          if (state is Authenticated) {
            BlocProvider.of<StationBloc>(context).add(GetStations());
            return NavigationView();
          } else {
            return const LoginView();
          }
        },
      ),
      routes: {
        NavigationView.routeName: (context) => NavigationView(),
        LoginView.routeName: (context) => const LoginView(),
        ProfileView.routeName: (context) => const ProfileView(),
        QrView.routeName: (context) => QrView(),
        ReportsView.routeName: (context) => const ReportsView(),
        HistoryView.routeName: (context) => HistoryView(),
        ContactsView.routeName: (context) => const ContactsView(),
        AddContactView.routeName: (context) => const AddContactView(),
      },
      onGenerateRoute: (settings) {
        if (settings.name == SearchResultView.routeName) {
          return MaterialPageRoute(
            builder: (_) => SearchResultView(
              key: ValueKey(DateTime.now()),
            ),
          );
        }
        return null;
      },
    );
  }
}
