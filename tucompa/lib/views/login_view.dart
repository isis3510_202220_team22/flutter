import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tucompa/bloc/auth/auth_bloc.dart';
import 'package:tucompa/bloc/network/network_bloc.dart';
import 'package:tucompa/bloc/user/user_bloc.dart' hide Loading;
import 'package:tucompa/views/navigation_view.dart' as nav;
import 'package:tucompa/widgets/carousel_indicator.dart';

import '../bloc/route/route_bloc.dart';

class LoginView extends StatefulWidget {
  static const routeName = '/login';

  const LoginView({super.key});

  @override
  State<LoginView> createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  final _formKey = GlobalKey<FormState>();
  var _userEmail = '';
  var _userPassword = '';

  void _trySubmit() {
    final isValid = _formKey.currentState?.validate() ?? false;
    FocusScope.of(context).unfocus();

    if (isValid) {
      _formKey.currentState!.save();
      BlocProvider.of<AuthBloc>(context).add(SignInRequested(
        _userEmail.trim(),
        _userPassword.trim(),
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener<AuthBloc, AuthState>(
        listener: (context, state) {
          if (state is Authenticated) {
            Navigator.of(context)
                .pushReplacementNamed(nav.NavigationView.routeName);
            BlocProvider.of<UserBloc>(context).add(FetchRequested(_userEmail));
          }
          if (state is AuthError) {
            _userEmail = state.email;
            _userPassword = state.password;
            ScaffoldMessenger.of(context).clearSnackBars();
            ScaffoldMessenger.of(context)
                .showSnackBar(SnackBar(content: Text(state.error)));
          }
        },
        child: SafeArea(
          child: SingleChildScrollView(
            child: BlocBuilder<NetworkBloc, NetworkState>(
              builder: (context, networkState) {
                return Column(
                  children: [
                    Container(
                      width: double.infinity,
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20)),
                        color: Color.fromRGBO(216, 229, 237, 1),
                      ),
                      height: (MediaQuery.of(context).size.height -
                              MediaQuery.of(context).viewPadding.top) *
                          0.7,
                      child: SingleChildScrollView(
                        child: Column(
                          children: [
                            const Padding(
                              padding: EdgeInsets.symmetric(vertical: 30.0),
                              child: Image(
                                  image: AssetImage(
                                      'assets/images/main-logo.png')),
                            ),
                            const SizedBox(
                              height: 15,
                            ),
                            BlocBuilder<AuthBloc, AuthState>(
                              builder: (context, state) {
                                if (state is UnAuthenticated) {
                                  return SizedBox(
                                      height: 140,
                                      child: CarouselWithIndicator());
                                }
                                if (state is Initial) {
                                  return Form(
                                    key: _formKey,
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 30),
                                      child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          TextFormField(
                                            key: const ValueKey('email'),
                                            maxLength: 40,
                                            validator: (value) {
                                              RegExp reg = RegExp(
                                                  r'[a-z]+([\.]?[a-z0-9]+)?@uniandes.edu.co');
                                              RegExp reg2 =
                                                  RegExp(r'@uniandes.edu.co');
                                              if (value == null ||
                                                  value.isEmpty ||
                                                  !reg.hasMatch(value)) {
                                                return 'Ingrese un correo válido.';
                                              }
                                              if (reg2
                                                          .allMatches(value)
                                                          .length >
                                                      1 ||
                                                  !value.endsWith(
                                                      '@uniandes.edu.co')) {
                                                return 'Ingrese un correo válido.';
                                              }
                                              return null;
                                            },
                                            keyboardType:
                                                TextInputType.emailAddress,
                                            decoration: const InputDecoration(
                                              labelText: 'Correo',
                                            ),
                                            initialValue: _userEmail,
                                            onSaved: (value) {
                                              _userEmail = value!;
                                            },
                                          ),
                                          TextFormField(
                                            key: const ValueKey('contraseña'),
                                            maxLength: 16,
                                            validator: (value) {
                                              if (value == null ||
                                                  value.isEmpty ||
                                                  value.length < 5) {
                                                return 'Ingrese una contraseña válida.';
                                              }
                                              return null;
                                            },
                                            decoration: const InputDecoration(
                                                labelText: 'Contraseña'),
                                            obscureText: true,
                                            initialValue: _userPassword,
                                            onSaved: (value) {
                                              _userPassword = value!;
                                            },
                                            onFieldSubmitted:
                                                networkState is ConnectedState
                                                    ? (value) {
                                                        _trySubmit();
                                                      }
                                                    : null,
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                }
                                if (state is Loading) {
                                  return const Center(
                                    child: CircularProgressIndicator(),
                                  );
                                }
                                return Container();
                              },
                            ),
                            BlocBuilder<AuthBloc, AuthState>(
                              builder: (context, state) {
                                if (state is UnAuthenticated) {
                                  return Container(
                                    margin: const EdgeInsets.symmetric(
                                        horizontal: 30),
                                    child: const Text(
                                      style: TextStyle(fontSize: 16.0),
                                      'Siéntete tranquilo, todos los usuarios de esta aplicación son estudiantes activos en la Universidad de los Andes',
                                    ),
                                  );
                                }
                                return Container();
                              },
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      height: (MediaQuery.of(context).size.height -
                              MediaQuery.of(context).viewPadding.top) *
                          0.3,
                      width: double.infinity,
                      color: const Color.fromRGBO(5, 94, 131, 1),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Center(
                            child: ElevatedButton(
                              style: ButtonStyle(
                                backgroundColor: networkState is ConnectedState
                                    ? MaterialStateProperty.all(Colors.white)
                                    : MaterialStateProperty.all(
                                        Colors.grey[300]),
                                shape: MaterialStateProperty.all<
                                    RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20),
                                  ),
                                ),
                              ),
                              onPressed: networkState is ConnectedState
                                  ? () {
                                      BlocProvider.of<RouteBloc>(context)
                                          .add(Initialize());
                                      if (context.read<AuthBloc>().state
                                          is Initial) {
                                        _trySubmit();
                                      } else {
                                        context
                                            .read<AuthBloc>()
                                            .add(StartLogin());
                                      }
                                    }
                                  : null,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                mainAxisSize: MainAxisSize.min,
                                children: const [
                                  Text('Ingresar con Uniandes',
                                      style: TextStyle(
                                        color: Color.fromRGBO(5, 94, 131, 1),
                                      )),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Image(
                                    image: AssetImage(
                                        'assets/images/uniandes-logo.png'),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          if (networkState is NotConnectedState)
                            Padding(
                              padding: const EdgeInsets.only(top: 10.0),
                              child: Text(
                                'Debe tener conexión a internet para poder ingresar',
                                style: TextStyle(
                                  fontSize: 15,
                                  color: Colors.grey[300],
                                ),
                              ),
                            )
                        ],
                      ),
                    )
                  ],
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}
