import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tucompa/bloc/auth/auth_bloc.dart' hide Loading;
import 'package:tucompa/bloc/contact/contact_bloc.dart' hide Loading;
import 'package:tucompa/bloc/network/network_bloc.dart';
import 'package:tucompa/bloc/report/report_bloc.dart';
import 'package:tucompa/bloc/station/station_bloc.dart';
import 'package:tucompa/bloc/user/user_bloc.dart';
import 'package:tucompa/bloc/user_routes/user_routes_bloc.dart';
import 'package:tucompa/views/history_view.dart';
import 'package:tucompa/views/contacts_view.dart';
import 'package:tucompa/views/login_view.dart';
import 'package:tucompa/views/qr_view.dart';
import 'package:tucompa/views/reports_view.dart';

class ProfileView extends StatefulWidget {
  static const routeName = '/profile';

  const ProfileView({super.key});

  @override
  State<ProfileView> createState() => _ProfileViewState();
}

class _ProfileViewState extends State<ProfileView> {
  bool image = false;
  bool trying = false;

  void showSnackBar(BuildContext ctx, String message) {
    ScaffoldMessenger.of(ctx).clearSnackBars();
    ScaffoldMessenger.of(ctx).showSnackBar(SnackBar(content: Text(message)));
  }

  @override
  Widget build(BuildContext context) {
    final FirebaseAnalytics analytics = FirebaseAnalytics.instance;
    Future<void> sendAnalyticsEvent(String eventName) async {
      await analytics.logEvent(name: eventName);
    }

    return SafeArea(
      child: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              padding: const EdgeInsets.only(top: 20),
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(20),
                  bottomRight: Radius.circular(20),
                ),
                color: Color.fromRGBO(216, 229, 237, 1),
              ),
              child: Column(
                children: [
                  BlocBuilder<UserBloc, UserState>(
                    builder: (ctx, state) {
                      if (state is Loading) {
                        return const CircleAvatar(
                          radius: 100,
                          child: CircularProgressIndicator(),
                        );
                      }
                      if (state is FetchUser) {
                        CachedNetworkImageProvider cachedImage =
                            CachedNetworkImageProvider(
                          state.image,
                          cacheKey: image ? null : state.email,
                        );
                        return Stack(children: [
                          CircleAvatar(
                            radius: 100,
                            foregroundImage: cachedImage,
                            backgroundImage: const AssetImage(
                                'assets/images/profile-placeholder.png'),
                            backgroundColor: const Color.fromRGBO(3, 69, 96, 1),
                            onForegroundImageError: (exception, stackTrace) {
                              setState(() {
                                image = true;
                              });
                            },
                            child: trying && !image
                                ? const Padding(
                                    padding: EdgeInsets.only(left: 10, top: 15),
                                    child: CircularProgressIndicator(),
                                  )
                                : Container(),
                          ),
                          Positioned(
                            top: 10,
                            right: 10,
                            child: image
                                ? InkWell(
                                    onTap: () {
                                      setState(() {
                                        image = false;
                                        trying = true;
                                      });
                                      if (context.read<NetworkBloc>().state
                                          is NotConnectedState) {
                                        showSnackBar(context,
                                            'No tiene internet para obtener la imagen');
                                      } else {
                                        showSnackBar(context,
                                            'Intentando obtener la imagen.');
                                      }
                                    },
                                    child: Container(
                                        width: 35,
                                        height: 35,
                                        decoration: const BoxDecoration(
                                          boxShadow: [
                                            BoxShadow(
                                                color: Colors.black54,
                                                blurRadius: 8)
                                          ],
                                          color: Colors.white,
                                          shape: BoxShape.circle,
                                        ),
                                        child:
                                            const Icon(Icons.replay_outlined)),
                                  )
                                : Container(),
                          ),
                        ]);
                      }
                      return Container();
                    },
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Column(
                    children: [
                      BlocBuilder<UserBloc, UserState>(
                        builder: (context, state) {
                          if (state is FetchUser) {
                            return Text(
                              state.name,
                              style: const TextStyle(fontSize: 25),
                            );
                          }
                          if (state is Loading) {
                            return const Text(
                              "-",
                              style: TextStyle(
                                  fontSize: 25, fontWeight: FontWeight.bold),
                            );
                          }
                          return Container();
                        },
                      ),
                      BlocBuilder<UserBloc, UserState>(
                        builder: (context, state) {
                          if (state is FetchUser) {
                            return Text(
                              state.email,
                              style: const TextStyle(fontSize: 20),
                            );
                          }
                          if (state is Loading) {
                            return const Text(
                              "-",
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.bold),
                            );
                          }
                          return Container();
                        },
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          BlocBuilder<UserBloc, UserState>(
                            builder: (context, state) {
                              if (state is FetchUser) {
                                return Text(
                                  state.quantityRating == 0
                                      ? '-'
                                      : (state.totalRating /
                                              state.quantityRating)
                                          .toStringAsFixed(2),
                                  style: const TextStyle(fontSize: 20),
                                );
                              }
                              return Container();
                            },
                          ),
                          const Icon(Icons.star),
                        ],
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                ],
              ),
            ),
            BlocBuilder<NetworkBloc, NetworkState>(
              builder: (context, networkState) {
                return Column(
                  children: [
                    const SizedBox(
                      height: 20,
                    ),
                    const Text(
                      'Racha',
                      style: TextStyle(fontSize: 20),
                    ),
                    BlocBuilder<UserBloc, UserState>(builder: (context, state) {
                      if (state is FetchUser) {
                        return Text(
                          state.streak > 1
                              ? '${state.streak} días'
                              : state.streak < 1
                                  ? '-'
                                  : '${state.streak} día',
                          style: const TextStyle(fontSize: 15),
                        );
                      }
                      return const Text(
                        '-',
                        style: TextStyle(fontSize: 15),
                      );
                    }),
                    const SizedBox(height: 30),
                    BlocBuilder<AuthBloc, AuthState>(builder: (context, state) {
                      String userEmail = '';
                      if (state is Authenticated) {
                        userEmail = state.email;
                      }
                      return TextButton.icon(
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all(
                              const Color.fromRGBO(216, 229, 237, 1)),
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20),
                            ),
                          ),
                        ),
                        onPressed: () {
                          BlocProvider.of<ContactBloc>(context)
                              .add(ContactsRequested(userEmail));
                          sendAnalyticsEvent('contacts_view_opened');
                          Navigator.of(context)
                              .pushNamed(ContactsView.routeName);
                        },
                        icon: const Icon(
                          Icons.groups_outlined,
                          color: Color.fromRGBO(3, 69, 96, 1),
                        ),
                        label: const Text(
                          'Contactos Favoritos',
                          style: TextStyle(
                            color: Color.fromRGBO(3, 69, 96, 1),
                            fontSize: 16,
                          ),
                        ),
                      );
                    }),
                    const SizedBox(
                      height: 10,
                    ),
                    BlocBuilder<UserBloc, UserState>(
                      builder: (context, state) {
                        if (state is FetchUser) {
                          return TextButton.icon(
                            style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all(
                                  const Color.fromRGBO(216, 229, 237, 1)),
                              shape: MaterialStateProperty.all<
                                  RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20),
                                ),
                              ),
                            ),
                            onPressed: () {
                              BlocProvider.of<UserRoutesBloc>(context)
                                  .add(FetchHistory(state.email));
                              Navigator.of(context)
                                  .pushNamed(HistoryView.routeName);
                            },
                            icon: const Icon(
                              Icons.history,
                              color: Color.fromRGBO(3, 69, 96, 1),
                            ),
                            label: const Text(
                              'Historial de Viajes',
                              style: TextStyle(
                                color: Color.fromRGBO(3, 69, 96, 1),
                                fontSize: 16,
                              ),
                            ),
                          );
                        }
                        return Container();
                      },
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        CircleAvatar(
                          backgroundColor:
                              const Color.fromRGBO(216, 229, 237, 1),
                          radius: 50,
                          child: IconButton(
                            onPressed: () {
                              Navigator.of(context)
                                  .pushNamed(ReportsView.routeName);
                              BlocProvider.of<StationBloc>(context)
                                  .add(InitializeStations());
                              BlocProvider.of<StationBloc>(context)
                                  .add(GetStations());
                            },
                            icon: const Icon(
                              Icons.flag_outlined,
                              size: 50,
                            ),
                            color: const Color.fromRGBO(3, 69, 96, 1),
                            padding: EdgeInsets.zero,
                          ),
                        ),
                        CircleAvatar(
                          backgroundColor:
                              const Color.fromRGBO(216, 229, 237, 1),
                          radius: 50,
                          child: IconButton(
                            onPressed: () {},
                            icon: const Icon(
                              Icons.emergency,
                              size: 50,
                            ),
                            color: Colors.red[700],
                            padding: EdgeInsets.zero,
                          ),
                        ),
                        CircleAvatar(
                          backgroundColor:
                              const Color.fromRGBO(216, 229, 237, 1),
                          radius: 50,
                          child: IconButton(
                            onPressed: () {
                              BlocProvider.of<StationBloc>(context)
                                  .add(InitializeStations());
                              Navigator.of(context).pushNamed(QrView.routeName);
                            },
                            icon: const Icon(
                              Icons.qr_code_scanner_outlined,
                              size: 50,
                            ),
                            color: const Color.fromRGBO(3, 69, 96, 1),
                            padding: EdgeInsets.zero,
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    TextButton.icon(
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(
                            const Color.fromRGBO(216, 229, 237, 1)),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20),
                          ),
                        ),
                      ),
                      onPressed: () {
                        if (networkState is NotConnectedState) {
                          ScaffoldMessenger.of(context).clearSnackBars();
                          ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(
                              content: Text(
                                  'Debe tener conexión a internet para poder cerrar sesión'),
                            ),
                          );
                          return;
                        }
                        Navigator.of(context)
                            .pushReplacementNamed(LoginView.routeName);
                        BlocProvider.of<AuthBloc>(context)
                            .add(SignOutRequested());
                        BlocProvider.of<UserBloc>(context).add(UserLogout());
                        BlocProvider.of<ReportBloc>(context).add(ExitReport());
                        BlocProvider.of<ContactBloc>(context)
                            .add(ExitContacts());
                      },
                      icon: const Icon(
                        Icons.logout,
                        color: Color.fromRGBO(3, 69, 96, 1),
                      ),
                      label: const Text(
                        'Cerrar sesión',
                        style: TextStyle(
                          color: Color.fromRGBO(3, 69, 96, 1),
                          fontSize: 16,
                        ),
                      ),
                    ),
                  ],
                );
              },
            )
          ],
        ),
      ),
    );
  }
}
