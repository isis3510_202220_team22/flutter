import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tucompa/bloc/network/network_bloc.dart';
import 'package:tucompa/bloc/station/station_bloc.dart';
import 'package:tucompa/bloc/user_routes/user_routes_bloc.dart';
import 'package:tucompa/views/search_result_view.dart';
import 'package:tucompa/widgets/stations.dart';

class SearchView extends StatefulWidget {
  static const routeName = '/search';
  const SearchView({super.key});

  @override
  State<SearchView> createState() => _SearchViewState();
}

class _SearchViewState extends State<SearchView> {
  String origin = 'Seleccione...';
  String destination = 'Seleccione...';
  List<String> loadedStations = [];
  bool? am;
  String? hour;
  String? minutes;
  DateTime? time;

  void _getTime(TimeOfDay t) {
    final now = DateTime.now();
    time = DateTime(now.year, now.month, now.day, t.hour, t.minute);
  }

  void _getOriginStation(String o) {
    origin = o;
  }

  void _getDestinationStation(String d) {
    destination = d;
  }

  void _setAm(bool a) {
    setState(() {
      am = a;
    });
  }

  void _setHour(String h) {
    setState(() {
      hour = h;
    });
  }

  void _setMinute(String m) {
    setState(() {
      minutes = m;
    });
  }

  void _showError(String error) {
    ScaffoldMessenger.of(context).clearSnackBars();
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(content: Text(error)),
    );
  }

  @override
  Widget build(BuildContext context) {
    if (loadedStations.isEmpty) {
      BlocProvider.of<StationBloc>(context).add(InitializeStations());
      BlocProvider.of<StationBloc>(context).add(GetStations());
    }
    return MultiBlocListener(
      listeners: [
        BlocListener<StationBloc, StationState>(listener: ((ctx, state) {
          if (state is LoadedStations) {
            setState(() {
              loadedStations = state.stations;
            });
          }
        })),
      ],
      child: SafeArea(
        child: Center(
          child: SingleChildScrollView(
            child: Column(
              children: [
                const Padding(
                  padding: EdgeInsets.only(
                    bottom: 50,
                  ),
                  child: Text(
                    'Buscar un viaje',
                    style: TextStyle(
                      color: Color.fromRGBO(3, 69, 96, 1),
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                    ),
                  ),
                ),
                Container(
                  width: double.infinity,
                  padding: const EdgeInsets.only(top: 60, bottom: 40),
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(20),
                    ),
                    color: Color.fromRGBO(132, 168, 184, 1),
                  ),
                  child: Column(
                    children: [
                      Stations(_getOriginStation, _getDestinationStation,
                          origin, destination),
                      const SizedBox(
                        height: 20,
                      ),
                      Container(
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? MediaQuery.of(context).size.width * 0.85
                            : MediaQuery.of(context).size.width * 0.43,
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(
                            Radius.circular(7),
                          ),
                          color: Color.fromRGBO(216, 229, 237, 1),
                        ),
                        child: Column(
                          children: [
                            const Padding(
                              padding: EdgeInsets.only(top: 8.0),
                              child: Text(
                                'Hora salida',
                                style: TextStyle(
                                  color: Color.fromRGBO(3, 69, 96, 1),
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                children: [
                                  const Padding(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 8.0),
                                    child: CircleAvatar(
                                      backgroundColor:
                                          Color.fromRGBO(3, 69, 96, 1),
                                      child: IconButton(
                                        onPressed: null,
                                        disabledColor: Colors.white,
                                        icon: Icon(
                                          Icons.schedule,
                                        ),
                                        color: Color.fromRGBO(216, 229, 237, 1),
                                        padding: EdgeInsets.zero,
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 8.0),
                                      child: TextButton(
                                        style: ButtonStyle(
                                          backgroundColor:
                                              MaterialStateProperty.all(
                                                  Colors.white),
                                        ),
                                        onPressed: () => showTimePicker(
                                          context: context,
                                          initialTime: time == null
                                              ? TimeOfDay.now()
                                              : TimeOfDay.fromDateTime(time!),
                                        ).then((value) {
                                          if (value == null) {
                                            return;
                                          }
                                          String pickedHour =
                                              value.hour.toString();
                                          if (int.parse(pickedHour) > 12) {
                                            pickedHour =
                                                (int.parse(pickedHour) - 12)
                                                    .toString();
                                            _setAm(false);
                                          } else if (int.parse(pickedHour) ==
                                              12) {
                                            _setAm(false);
                                          } else {
                                            _setAm(true);
                                          }
                                          _setHour(pickedHour);
                                          String pickedMinute =
                                              value.minute.toString();
                                          if (pickedMinute.length == 1) {
                                            pickedMinute = '0$pickedMinute';
                                          }
                                          _setMinute(pickedMinute);
                                          _getTime(value);
                                        }),
                                        child: Text(
                                          time == null
                                              ? 'Seleccione...'
                                              : '$hour:$minutes ${am! ? 'AM' : 'PM'}',
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: time == null ? 16 : 20,
                                              fontWeight: FontWeight.w400),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      BlocBuilder<NetworkBloc, NetworkState>(
                        builder: (context, networkState) => TextButton(
                            onPressed: () {
                              if (networkState is NotConnectedState) {
                                ScaffoldMessenger.of(context).clearSnackBars();
                                ScaffoldMessenger.of(context).showSnackBar(
                                  const SnackBar(
                                    content: Text(
                                        'Debe tener conexión a internet para poder realizar una búsqueda'),
                                  ),
                                );
                                return;
                              }
                              if (origin == 'Seleccione...' ||
                                  destination == 'Seleccione...') {
                                _showError(
                                    'Debe ingresar la estación de origen y de destino');
                                return;
                              }
                              if (origin == destination) {
                                _showError(
                                    'La estación de origen y destino no pueden ser iguales');
                                return;
                              }
                              if (time == null) {
                                _showError('Debe ingresar una hora');
                                return;
                              }
                              if (time!.isBefore(DateTime.now())) {
                                _showError(
                                    'Debe ingresar una hora en el futuro');
                                return;
                              }
                              BlocProvider.of<UserRoutesBloc>(context)
                                  .add(FetchRoutes(origin, destination, time!));
                              Navigator.of(context)
                                  .pushNamed(SearchResultView.routeName);
                            },
                            style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all(
                                const Color.fromRGBO(3, 69, 96, 1),
                              ),
                              shape: MaterialStateProperty.all<
                                  RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20),
                                ),
                              ),
                            ),
                            child: const Padding(
                              padding: EdgeInsets.symmetric(horizontal: 20.0),
                              child: Text(
                                'Buscar',
                                style: TextStyle(color: Colors.white),
                              ),
                            )),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
