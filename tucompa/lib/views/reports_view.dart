import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tucompa/bloc/network/network_bloc.dart';
import 'package:tucompa/bloc/report/report_bloc.dart';
import 'package:tucompa/bloc/station/station_bloc.dart';
import 'package:tucompa/models/report.dart';
import 'package:tucompa/widgets/report_button.dart';
import 'package:tucompa/widgets/report_dropdown.dart';

class ReportsView extends StatefulWidget {
  static const routeName = '/reports';
  const ReportsView({super.key});

  @override
  State<ReportsView> createState() => _ReportsViewState();
}

class _ReportsViewState extends State<ReportsView> {
  List<String> reportTypes = [
    '¿Qué tipo de situación?',
    'Robo',
    'Acoso',
    'Actividad sospechosa',
    'Otro'
  ];
  DateTime? date;
  TimeOfDay? time;
  String? selectedType;
  String? selectedStation;
  TextEditingController controller = TextEditingController();

  void _selectType(BuildContext ctx, String selected) {
    selectedType = selected;
    BlocProvider.of<ReportBloc>(ctx).add(SaveReport(
        type: selected,
        date: date,
        time: time,
        description: controller.text,
        station: selectedStation));
  }

  void _selectStation(BuildContext ctx, String selected) {
    selectedStation = selected;
    BlocProvider.of<ReportBloc>(ctx).add(SaveReport(
        type: selectedType,
        date: date,
        time: time,
        description: controller.text,
        station: selected));
  }

  void _selectDate(BuildContext ctx) {
    showDatePicker(
      context: ctx,
      initialDate: DateTime.now(),
      firstDate: DateTime(2022),
      lastDate: DateTime.now(),
    ).then((value) {
      if (value != null) {
        setState(() {
          date = value;
        });
        BlocProvider.of<ReportBloc>(ctx).add(SaveReport(
            type: selectedType,
            date: value,
            time: time,
            description: controller.text,
            station: selectedStation));
      }
    });
  }

  void _selectTime(BuildContext ctx) {
    showTimePicker(
      context: ctx,
      initialTime: TimeOfDay.now(),
    ).then((value) {
      if (value != null) {
        setState(() {
          time = value;
        });
        BlocProvider.of<ReportBloc>(ctx).add(SaveReport(
            type: selectedType,
            date: date,
            time: value,
            description: controller.text,
            station: selectedStation));
      }
    });
  }

  String dateString() {
    return '${date!.day}/${date!.month}/${date!.year}';
  }

  String timeString() {
    String am = time!.hour > 11 ? 'PM' : 'AM';
    int hour = time!.hour > 12 ? time!.hour - 12 : time!.hour;
    String minute =
        time!.minute < 10 ? '0${time!.minute}' : time!.minute.toString();
    return '$hour:$minute $am';
  }

  void sendReport(BuildContext ctx) {
    if (selectedType == null || selectedType == '¿Qué tipo de situación?') {
      showSnackBar(ctx, 'Debe seleccionar un tipo de situación');
      return;
    }
    if (date == null) {
      showSnackBar(ctx, 'Debe seleccionar una fecha');
      return;
    }
    if (time == null) {
      showSnackBar(ctx, 'Debe seleccionar una hora');
      return;
    }
    if (selectedStation == null ||
        selectedStation == 'Lugar de los hechos...') {
      showSnackBar(ctx, 'Debe seleccionar una estación');
      return;
    }
    if (controller.text == '' || controller.text.trim() == '') {
      showSnackBar(ctx, 'Debe llenar la descripción de la situación');
      return;
    }
    if (controller.text.trim().length < 20) {
      showSnackBar(
          ctx, 'La descripción es muy corta, ingrese mínimo 20 caracteres');
      return;
    }
    bool isNum;
    try {
      int.parse(controller.text.trim());
      isNum = true;
    } catch (e) {
      isNum = false;
    }
    if (isNum) {
      showSnackBar(ctx, 'La descripción no puede ser numérica');
      return;
    }
    DateTime dateTime =
        DateTime(date!.year, date!.month, date!.day, time!.hour, time!.minute);
    if (dateTime.isAfter(DateTime.now())) {
      showSnackBar(ctx, 'No puede crear un reporte en el futuro');
      return;
    }
    Report report = Report(selectedType!, dateTime, controller.text);
    BlocProvider.of<ReportBloc>(ctx)
        .add(AddReport(report, selectedStation!.replaceAll(' ', '')));
  }

  void showSnackBar(BuildContext ctx, String message) {
    ScaffoldMessenger.of(ctx).clearSnackBars();
    ScaffoldMessenger.of(ctx).showSnackBar(SnackBar(content: Text(message)));
  }

  Future<bool> _onWillPop(BuildContext ctx) async {
    FocusManager.instance.primaryFocus?.unfocus();
    return (await showDialog(
          context: ctx,
          builder: (ct) => AlertDialog(
            title: const Text('Regresar'),
            content: const Text('¿Está seguro de regresar?'),
            actions: <Widget>[
              TextButton(
                onPressed: () => Navigator.of(ctx).pop(false),
                child: const Text('No'),
              ),
              TextButton(
                onPressed: () {
                  Navigator.of(ctx).pop(true);
                  ReportState state = ctx.read<ReportBloc>().state;
                  if (state is! ReportInitial) {
                    BlocProvider.of<ReportBloc>(ctx).add(ExitReport());
                  }
                },
                child: const Text('Si'),
              ),
            ],
          ),
        )) ??
        false;
  }

  @override
  Widget build(BuildContext context) {
    ReportState initStatus = context.read<ReportBloc>().state;
    if (initStatus is ReportInitial) {
      selectedType = initStatus.type;
      date = initStatus.date;
      time = initStatus.time;
      selectedStation = initStatus.station;
      controller.text = initStatus.description != null ? initStatus.description! : '';
    }
    return WillPopScope(
      onWillPop: () => _onWillPop(context),
      child: GestureDetector(
        onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
        child: Scaffold(
          body: SafeArea(
            child: SingleChildScrollView(
              child: SizedBox(
                width: double.infinity,
                child: Column(
                  children: [
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(5),
                          child: CircleAvatar(
                            backgroundColor: const Color.fromRGBO(3, 69, 105, 1),
                            child: IconButton(
                              onPressed: () => _onWillPop(context).then((value) {
                                if (value) {
                                  Navigator.of(context).pop();
                                }
                              }),
                              icon: const Icon(
                                Icons.arrow_back,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                        const Padding(
                          padding: EdgeInsets.only(
                            top: 5,
                            left: 24,
                          ),
                          child: Text(
                            'Reportar situación anómala',
                            style: TextStyle(
                              color: Color.fromRGBO(3, 69, 96, 1),
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 10.0, horizontal: 30),
                      child: Card(
                        color: const Color.fromRGBO(216, 229, 237, 1),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 20),
                          child: Column(
                            children: const [
                              Text('¿Para qué sirve esta herramienta?'),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                  'Reportar situaciones anómalas que ocurren dentro o alrededor de las estaciones del sistema Transmilenio.'),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                  'Describe de la manera más detallada los hechos para alertar a la comunidad.'),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 10.0, horizontal: 30),
                      child: Card(
                        color: const Color.fromRGBO(216, 229, 237, 1),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 20),
                          child: BlocBuilder<ReportBloc, ReportState>(
                              builder: (ctx, state) {
                            if (state is ReportAdded) {
                              return Column(
                                children: [
                                  const Text('Reporte agregado exitosamente'),
                                  TextButton(
                                    onPressed: () {
                                      Navigator.of(ctx).pop();
                                      BlocProvider.of<ReportBloc>(context)
                                          .add(ExitReport());
                                    },
                                    style: ButtonStyle(
                                      backgroundColor: MaterialStateProperty.all(
                                        const Color.fromRGBO(3, 69, 96, 1),
                                      ),
                                      shape: MaterialStateProperty.all<
                                          RoundedRectangleBorder>(
                                        RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(20),
                                        ),
                                      ),
                                    ),
                                    child: const Text(
                                      'Regresar',
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ),
                                ],
                              );
                            }
                            Widget sendButton =
                                BlocBuilder<NetworkBloc, NetworkState>(
                              builder: (context, networkState) {
                                return TextButton(
                                  onPressed: () {
                                    if (networkState is NotConnectedState) {
                                      ScaffoldMessenger.of(context)
                                          .clearSnackBars();
                                      ScaffoldMessenger.of(context).showSnackBar(
                                        const SnackBar(
                                          content: Text(
                                              'Debe tener conexión a internet para poder enviar un reporte'),
                                        ),
                                      );
                                      return;
                                    }
                                    sendReport(context);
                                  },
                                  style: ButtonStyle(
                                    backgroundColor: MaterialStateProperty.all(
                                      const Color.fromRGBO(3, 69, 96, 1),
                                    ),
                                    shape: MaterialStateProperty.all<
                                        RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(20),
                                      ),
                                    ),
                                  ),
                                  child: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: const [
                                      Text(
                                        'Enviar',
                                        style: TextStyle(color: Colors.white),
                                      ),
                                      Icon(Icons.arrow_forward,
                                          color: Colors.white),
                                    ],
                                  ),
                                );
                              },
                            );
                            int selectedTypeInit = 0;
                            if (state is ReportInitial && state.type != null) {
                              selectedTypeInit = reportTypes.indexOf(state.type!);
                            }
                            if (state is ReportError && state.type != null) {
                              selectedTypeInit = reportTypes.indexOf(state.type!);
                            }
                            if (state is ReportLoading && state.type != null) {
                              selectedTypeInit = reportTypes.indexOf(state.type!);
                            }
                            return Column(
                              children: [
                                ReportDropdown(
                                  reportTypes,
                                  _selectType,
                                  selectedItem: selectedTypeInit,
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                ReportButton(
                                  _selectDate,
                                  date != null
                                      ? dateString()
                                      : 'Fecha de la situación',
                                  const Icon(Icons.calendar_month_outlined),
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                ReportButton(
                                  _selectTime,
                                  time != null
                                      ? timeString()
                                      : 'Hora de la situación',
                                  const Icon(Icons.schedule),
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                BlocBuilder<StationBloc, StationState>(
                                  builder: (context, st) {
                                    if (st is LoadedStations) {
                                      List<String> stations = st.stations;
                                      if (stations.contains('Seleccione...')) {
                                        stations.remove('Seleccione...');
                                      }
                                      if (!stations
                                          .contains('Lugar de los hechos...')) {
                                        stations.insert(
                                            0, 'Lugar de los hechos...');
                                      }
                                      int selectedStationInit = 0;
                                      if (state is ReportInitial &&
                                          state.station != null) {
                                        selectedStationInit =
                                            stations.map((e) => e.replaceAll(' ', '')).toList().indexOf(state.station!.replaceAll(' ', ''));
                                      }
                                      if (state is ReportError &&
                                          state.station != null) {
                                        selectedStationInit =
                                            stations.map((e) => e.replaceAll(' ', '')).toList().indexOf(state.station!.replaceAll(' ', ''));
                                      }
                                      if (state is ReportLoading &&
                                          state.station != null) {
                                        selectedStationInit =
                                            stations.map((e) => e.replaceAll(' ', '')).toList().indexOf(state.station!.replaceAll(' ', ''));
                                      }
                                      return ReportDropdown(
                                        stations,
                                        _selectStation,
                                        selectedItem: selectedStationInit,
                                      );
                                    }
                                    return ReportDropdown(
                                        const ['Lugar de los hechos...'],
                                        _selectStation);
                                  },
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: const [
                                    Text(
                                      'Descripción',
                                      style: TextStyle(fontSize: 16),
                                    )
                                  ],
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                TextField(
                                  controller: controller,
                                  maxLength: 250,
                                  maxLengthEnforcement:
                                      MaxLengthEnforcement.enforced,
                                  decoration: InputDecoration(
                                    filled: true,
                                    fillColor:
                                        const Color.fromRGBO(242, 244, 245, 1),
                                    border: OutlineInputBorder(
                                      borderSide: BorderSide.none,
                                      borderRadius: BorderRadius.circular(15),
                                    ),
                                    contentPadding: const EdgeInsets.symmetric(
                                      horizontal: 10,
                                      vertical: 10,
                                    ),
                                    hintText:
                                        'Describe la situación que presenciaste',
                                  ),
                                  minLines: 5,
                                  maxLines: 10,
                                  onChanged: (value) {
                                    BlocProvider.of<ReportBloc>(ctx).add(
                                        SaveReport(
                                            type: selectedType,
                                            date: date,
                                            time: time,
                                            description: controller.text,
                                            station: selectedStation));
                                  },
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                if (state is ReportInitial ||
                                    state is ReportError)
                                  sendButton,
                                if (state is ReportLoading)
                                  const CircularProgressIndicator(),
                                if (state is ReportError) Text(state.error),
                              ],
                            );
                          }),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
