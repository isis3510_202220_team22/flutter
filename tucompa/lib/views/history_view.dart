import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tucompa/bloc/user_routes/user_routes_bloc.dart';
import 'package:tucompa/widgets/history_card.dart';

import '../bloc/auth/auth_bloc.dart';
import '../bloc/network/network_bloc.dart';
import '../bloc/user/user_bloc.dart';

class HistoryView extends StatefulWidget {
  static const routeName = '/history';

  @override
  _HistoryViewState createState() => _HistoryViewState();
}

class _HistoryViewState extends State<HistoryView> {
  @override
  Widget build(BuildContext context) {
    String userEmail = '';
    if (context.read<AuthBloc>().state is Authenticated) {
      userEmail = context.read<AuthBloc>().state.props[0].toString();
    }
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        leading: Padding(
          padding: const EdgeInsets.only(
            left: 5,
            top: 10,
            right: 5,
          ),
          child: CircleAvatar(
            backgroundColor: const Color.fromRGBO(3, 69, 105, 1),
            child: IconButton(
              onPressed: () {
                BlocProvider.of<UserRoutesBloc>(context).add(ExitView());
                Navigator.of(context).pop();
              },
              icon: const Icon(
                Icons.arrow_back,
                color: Colors.white,
              ),
            ),
          ),
        ),
        title: const Padding(
          padding: EdgeInsets.only(top: 10),
          child: Text(
            'Historial de viajes',
            style: TextStyle(
              color: Color.fromRGBO(3, 69, 96, 1),
              fontWeight: FontWeight.bold,
              fontSize: 20,
            ),
          ),
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          BlocBuilder<NetworkBloc, NetworkState>(
            builder: ((context, networkState) {
              if (networkState is ConnectedState) {
                BlocProvider.of<UserRoutesBloc>(context)
                    .add(FetchHistory(userEmail));
              }
              if (networkState is NotConnectedState) {
                return Padding(
                  padding: EdgeInsets.only(
                    left: MediaQuery.of(context).orientation ==
                            Orientation.portrait
                        ? 0
                        : 80,
                    top: 25,
                  ),
                  child: Container(
                    height: MediaQuery.of(context).orientation ==
                            Orientation.portrait
                        ? MediaQuery.of(context).size.height * 0.04
                        : MediaQuery.of(context).size.height * 0.07,
                    width: MediaQuery.of(context).orientation ==
                            Orientation.portrait
                        ? MediaQuery.of(context).size.width * 0.55
                        : MediaQuery.of(context).size.width * 0.3,
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(
                        Radius.circular(7),
                      ),
                      color: Color.fromRGBO(216, 229, 237, 1),
                    ),
                    child: const Align(
                      alignment: Alignment.center,
                      child: Text(
                        'No tiene conexión a internet!',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Color.fromRGBO(3, 69, 96, 1),
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                );
              }
              return Container();
            }),
          ),
          BlocBuilder<UserRoutesBloc, UserRoutesState>(
            builder: (context, state) {
              if (state is UserRoutesError) {
                return Padding(
                  padding: EdgeInsets.only(
                    left: MediaQuery.of(context).orientation ==
                            Orientation.portrait
                        ? 50
                        : 80,
                    top: 25,
                  ),
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.8,
                    height: 50,
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(
                        Radius.circular(7),
                      ),
                      color: Color.fromRGBO(216, 229, 237, 1),
                    ),
                    child: Align(
                      alignment: Alignment.center,
                      child: Text(
                        state.error,
                        textAlign: TextAlign.center,
                        style: const TextStyle(
                          color: Color.fromRGBO(3, 69, 96, 1),
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                );
              }
              if (state is UserRoutesLoading) {
                return Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: const [
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: CircleAvatar(
                          backgroundColor: Colors.transparent,
                          radius: 100,
                          child: CircularProgressIndicator(),
                        ),
                      ),
                    ]);
              }
              if (state is History) {
                if (state.routes.isEmpty) {
                  return Card(
                    color: const Color.fromRGBO(216, 229, 237, 1),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      children: const [
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: 10.0),
                          child: Text(
                            'No tiene viajes en su historial',
                            style: TextStyle(fontSize: 15),
                          ),
                        ),
                      ],
                    ),
                  );
                }
                return Expanded(
                  child: SingleChildScrollView(
                    child: ListView.builder(
                      physics: const NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: state.routes.length,
                      itemBuilder: (context, index) {
                        return StreamBuilder(
                          stream: BlocProvider.of<UserBloc>(context)
                              .getUserInfo(state.routes[index].userEmail),
                          builder:
                              (context, AsyncSnapshot<QuerySnapshot> snapshot) {
                            if (snapshot.data != null) {
                              return Padding(
                                padding: const EdgeInsets.only(
                                  top: 20,
                                  left: 35,
                                  right: 35,
                                  bottom: 5,
                                ),
                                child: HistoryCard(
                                  state.routes[index].departureDate
                                      .toString()
                                      .substring(0, 16),
                                  state.routes[index].originStationName,
                                  state.routes[index].destinationStationName,
                                  snapshot.data!.docs[0].get('name'),
                                  snapshot.data!.docs[0].get('image'),
                                  key: ValueKey(DateTime.now()),
                                ),
                              );
                            }
                            return Container();
                          },
                        );
                      },
                    ),
                  ),
                );
              }
              return Container();
            },
          ),
        ],
      ),
    );
  }
}
