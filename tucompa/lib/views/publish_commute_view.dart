import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:location/location.dart';
import 'package:tucompa/bloc/auth/auth_bloc.dart';
import 'package:tucompa/bloc/route/route_bloc.dart';
import 'package:tucompa/bloc/station/station_bloc.dart';
import 'package:tucompa/views/current_commute_view.dart';
import 'package:tucompa/views/rating_view.dart';
import 'package:tucompa/widgets/hour.dart';
import 'package:tucompa/widgets/stations.dart';

import '../bloc/network/network_bloc.dart';
import '../bloc/user/user_bloc.dart';
import '../models/route.dart';
import 'package:firebase_performance/firebase_performance.dart';

class PublishCommuteView extends StatefulWidget {
  bool requestedService = false;
  bool requestedPermission = false;
  Map<String, dynamic> lastSuggestion = {
    'origin': 'None',
    'destination': 'None',
    'accepted': false
  };
  @override
  State<PublishCommuteView> createState() => _PublishCommuteViewState();
}

class _PublishCommuteViewState extends State<PublishCommuteView> {
  List<String> loadedStations = [];
  String userEmail = '';
  bool showedSuggestion = false;
  String origin = 'Seleccione...';
  String destination = 'Seleccione...';

  var location = Location();

  Future<Map<String, double>> getSuggestion() async {
    bool service = await location.serviceEnabled();
    var permission = await location.hasPermission();
    if (permission == PermissionStatus.denied ||
        permission == PermissionStatus.deniedForever ||
        !service) {
      throw Error();
    }
    var currentLocation = await location.getLocation();
    return {
      'latitude': currentLocation.latitude!,
      'longitude': currentLocation.longitude!
    };
  }

  Future<bool> _showSuggestionDialog(
      BuildContext context, String originSt, String? destinationSt) async {
    bool accept = false;
    String lastOrigin = widget.lastSuggestion['origin'];
    String lastDestination = widget.lastSuggestion['destination'];
    if (lastOrigin == originSt &&
        ((destinationSt == null && lastDestination == 'None') ||
            lastDestination == destinationSt)) {
      return false;
    }
    await showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Row(
            mainAxisSize: MainAxisSize.min,
            children: const [
              Text('Sugerencia'),
              SizedBox(
                width: 10,
              ),
              Icon(Icons.bus_alert),
            ],
          ),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(destinationSt == null
                    ? 'De acuerdo con su ubicación, le sugerimos empezar su ruta en la siguiente estación:'
                    : 'De acuerdo con su ubicación y sus rutas frecuentes, le sugerimos esta ruta:'),
                const SizedBox(
                  height: 10,
                ),
                Text(
                  destinationSt == null ? originSt : 'Origen: $originSt',
                ),
                if (destinationSt != null)
                  Text(
                    'Destino: $destinationSt',
                  ),
                const SizedBox(
                  height: 10,
                ),
                const Text('¿Quiere usar esta sugerencia?'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('Sí'),
              onPressed: () {
                Navigator.of(context).pop();
                accept = true;
              },
            ),
            TextButton(
              child: const Text('No'),
              onPressed: () {
                Navigator.of(context).pop();
                accept = false;
              },
            ),
          ],
        );
      },
    );
    widget.lastSuggestion['origin'] = originSt;
    widget.lastSuggestion['destination'] = destinationSt ?? 'None';
    widget.lastSuggestion['accepted'] = accept;
    return accept;
  }

  @override
  Widget build(BuildContext context) {
    DateTime time = DateTime.now();
    String minutes = TimeOfDay.now().minute.toString().length == 1
        ? '0${TimeOfDay.now().minute}'
        : TimeOfDay.now().minute.toString();
    bool am = TimeOfDay.now().hour < 12;
    String hour = TimeOfDay.now().hour > 12
        ? (TimeOfDay.now().hour - 12).toString()
        : TimeOfDay.now().hour.toString();

    if (context.read<AuthBloc>().state is Authenticated) {
      userEmail = context.read<AuthBloc>().state.props[0].toString();
    }

    void getTime(TimeOfDay t) {
      final now = DateTime.now();
      time = DateTime(now.year, now.month, now.day, t.hour, t.minute);
    }

    void getOriginStation(String o) {
      origin = o;
    }

    void getDestinationStation(String d) {
      destination = d;
    }

    Future.delayed(
      const Duration(seconds: 1),
      () async {
        if (mounted) {
          bool service = await location.serviceEnabled();
          if (!widget.requestedService) {
            if (!service) {
              service = await location.requestService();
            }
            widget.requestedService = true;
          }
          if (service && !widget.requestedPermission) {
            var permissionGranted = await location.hasPermission();
            if (permissionGranted == PermissionStatus.denied) {
              await location.requestPermission();
            }
            widget.requestedPermission = true;
          }
          await location.requestPermission();

          BlocProvider.of<StationBloc>(context).add(InitializeStations());
          BlocProvider.of<StationBloc>(context).add(GetStations());
        }
      },
    );

    return StreamBuilder(
      stream: BlocProvider.of<UserBloc>(context).getUserInfo(userEmail),
      builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.data == null || snapshot.data!.size == 0) {
          const CircleAvatar(
            radius: 100,
            child: CircularProgressIndicator(),
          );
        } else {
          if (snapshot.data!.docs[0].get('isRating') == true) {
            BlocProvider.of<RouteBloc>(context)
                .add(RateRoute(snapshot.data!.docs[0].get('currentRoute')));
          } else if (snapshot.data!.docs[0].get('isInRoute') == true &&
              snapshot.data!.docs[0].get('currentRoute') != "") {
            BlocProvider.of<RouteBloc>(context)
                .add(CheckRoute(snapshot.data!.docs[0].get('currentRoute')));
          } else {
            BlocProvider.of<RouteBloc>(context).add(Initialize());
          }
          return MultiBlocListener(
            listeners: [
              BlocListener<StationBloc, StationState>(
                listener: ((ctx, state) {
                  if (state is LoadedStations) {
                    if (loadedStations.isEmpty) {
                      setState(() {
                        loadedStations = state.stations;
                      });
                    }

                    if (context.read<RouteBloc>().state is! CurrentRoute &&
                        !showedSuggestion) {
                      getSuggestion().then((value) {
                        if (mounted) {
                          BlocProvider.of<StationBloc>(context).add(
                              GetSuggestion(userEmail, value['latitude']!,
                                  value['longitude']!));
                        }
                      }).onError((error, stackTrace) => null);
                    }
                  }
                  if (state is Suggestion &&
                      context.read<RouteBloc>().state is! CurrentRoute) {
                    if (state.suggestion.containsKey('origin')) {
                      if (!ModalRoute.of(context)!.isCurrent) {
                        Navigator.of(context).pop();
                      }
                      _showSuggestionDialog(
                              context,
                              state.suggestion['origin']!,
                              state.suggestion.containsKey('destination')
                                  ? state.suggestion['destination']
                                  : null)
                          .then(
                        (value) {
                          if (value) {
                            setState(() {
                              origin = state.suggestion['origin']!;
                            });
                            if (state.suggestion.containsKey('destination')) {
                              setState(() {
                                destination = state.suggestion['destination']!;
                              });
                            }
                          }
                        },
                      ).then((value) {
                        showedSuggestion = true;
                        BlocProvider.of<StationBloc>(context)
                            .add(ExitSuggestion(loadedStations));
                      });
                    }
                  }
                }),
              ),
              BlocListener<RouteBloc, RouteState>(
                listener: (context, state) {
                  if (state is RouteError) {
                    ScaffoldMessenger.of(context).clearSnackBars();
                    ScaffoldMessenger.of(context)
                        .showSnackBar(SnackBar(content: Text(state.error)));
                  }
                },
              ),
            ],
            child: BlocBuilder<RouteBloc, RouteState>(
              builder: (ctx, state) {
                if (state is CurrentRoute && state.routeData[5] == 'false') {
                  return CurrentCommuteView();
                } else if (state is CurrentRoute &&
                    snapshot.data!.docs[0].get('isRating') == true) {
                  return RatingView(
                    routeId: state.routeData[0],
                    email: userEmail,
                  );
                } else {
                  return OrientationBuilder(
                    builder: (context, orientation) {
                      return GridView.count(
                        childAspectRatio: orientation == Orientation.portrait
                            ? MediaQuery.of(context).size.width /
                                ((MediaQuery.of(context).size.height - 90) / 2)
                            : MediaQuery.of(context).size.width /
                                (MediaQuery.of(context).size.height * 1.6),
                        crossAxisCount:
                            orientation == Orientation.portrait ? 1 : 2,
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              const Padding(
                                padding: EdgeInsets.only(
                                  top: 5,
                                  left: 24,
                                  bottom: 10,
                                ),
                                child: Text(
                                  'Publicar un viaje',
                                  style: TextStyle(
                                    color: Color.fromRGBO(3, 69, 96, 1),
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20,
                                  ),
                                ),
                              ),
                              Container(
                                margin: const EdgeInsets.only(left: 30),
                                padding: const EdgeInsets.all(10),
                                decoration: const BoxDecoration(
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(7),
                                  ),
                                  color: Color.fromRGBO(216, 229, 237, 1),
                                ),
                                child: const Text(
                                  'Hora',
                                  style: TextStyle(
                                    color: Color.fromRGBO(3, 69, 96, 1),
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                              BlocBuilder<NetworkBloc, NetworkState>(
                                builder: ((context, networkState) {
                                  if (networkState is NotConnectedState) {
                                    return Padding(
                                      padding: const EdgeInsets.only(left: 100),
                                      child: Container(
                                        height:
                                            orientation == Orientation.portrait
                                                ? MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    0.04
                                                : MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    0.07,
                                        width:
                                            orientation == Orientation.portrait
                                                ? MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.55
                                                : MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.3,
                                        decoration: const BoxDecoration(
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(7),
                                          ),
                                          color:
                                              Color.fromRGBO(216, 229, 237, 1),
                                        ),
                                        child: const Align(
                                          alignment: Alignment.center,
                                          child: Text(
                                            'No tiene conexión a internet!',
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              color:
                                                  Color.fromRGBO(3, 69, 96, 1),
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ),
                                      ),
                                    );
                                  }
                                  return Container();
                                }),
                              ),
                              Hour(getTime, hour, minutes, am),
                            ],
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width,
                            decoration: const BoxDecoration(
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(20),
                                topRight: Radius.circular(20),
                              ),
                              color: Color.fromRGBO(132, 168, 184, 1),
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  margin: const EdgeInsets.only(
                                    left: 30,
                                    top: 20,
                                  ),
                                  padding: const EdgeInsets.all(10),
                                  decoration: const BoxDecoration(
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(7),
                                    ),
                                    color: Color.fromRGBO(216, 229, 237, 1),
                                  ),
                                  child: const Text(
                                    'Estaciones',
                                    style: TextStyle(
                                      color: Color.fromRGBO(3, 69, 96, 1),
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                    left: 30,
                                    top: 20,
                                  ),
                                  child: Stations(
                                      getOriginStation,
                                      getDestinationStation,
                                      widget.lastSuggestion['accepted'] &&
                                              widget.lastSuggestion['origin'] !=
                                                  'None'
                                          ? widget.lastSuggestion['origin']
                                          : origin,
                                      widget.lastSuggestion['accepted'] &&
                                              widget.lastSuggestion[
                                                      'destination'] !=
                                                  'None'
                                          ? widget.lastSuggestion['destination']
                                          : destination),
                                ),
                                BlocBuilder<RouteBloc, RouteState>(
                                  builder: (ctx, state) {
                                    if (state is RouteLoading) {
                                      return const Center(
                                        child: Padding(
                                          padding: EdgeInsets.only(top: 20),
                                          child: CircularProgressIndicator(),
                                        ),
                                      );
                                    }
                                    if (state is RouteAdded) {
                                      return const Center(
                                        child: Padding(
                                          padding: EdgeInsets.only(top: 20),
                                          child: Text(
                                            'Tu viaje ha sido publicado',
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              color:
                                                  Color.fromRGBO(3, 69, 96, 1),
                                              fontSize: 18,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ),
                                      );
                                    }

                                    if (state is RouteError ||
                                        state is RouteInicial) {
                                      return Container(
                                        height: 40,
                                        width: 210,
                                        margin: const EdgeInsets.only(
                                          left: 100,
                                          top: 20,
                                        ),
                                        padding: const EdgeInsets.all(10),
                                        decoration: const BoxDecoration(
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(30),
                                          ),
                                          color:
                                              Color.fromRGBO(216, 229, 237, 1),
                                        ),
                                        child: BlocBuilder<NetworkBloc,
                                            NetworkState>(
                                          builder: ((context, networkState) {
                                            if (networkState
                                                is NotConnectedState) {
                                              return InkWell(
                                                onTap: () {
                                                  ScaffoldMessenger.of(context)
                                                      .clearSnackBars();
                                                  ScaffoldMessenger.of(context)
                                                      .showSnackBar(
                                                    const SnackBar(
                                                      content: Text(
                                                          'Debe tener conexión a internet para poder publicar un viaje '),
                                                    ),
                                                  );
                                                },
                                                child: const Text(
                                                  'Publicar viaje',
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                    color: Color.fromRGBO(
                                                        3, 69, 96, 1),
                                                    fontSize: 18,
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                ),
                                              );
                                            } else {
                                              return InkWell(
                                                onTap: () async {
                                                  Trace customTrace =
                                                      FirebasePerformance
                                                          .instance
                                                          .newTrace(
                                                              'custom-trace-commute');
                                                  await customTrace.start();
                                                  RouteModel route = RouteModel(
                                                      time,
                                                      origin,
                                                      destination,
                                                      userEmail);
                                                  BlocProvider.of<RouteBloc>(
                                                          context)
                                                      .add(AddRoute(
                                                          route, userEmail));
                                                  await customTrace.stop();
                                                  setState(() {
                                                    loadedStations = [
                                                      ...loadedStations
                                                    ];
                                                  });
                                                },
                                                child: const Text(
                                                  'Publicar viaje',
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                    color: Color.fromRGBO(
                                                        3, 69, 96, 1),
                                                    fontSize: 18,
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                ),
                                              );
                                            }
                                          }),
                                        ),
                                      );
                                    }
                                    return Container();
                                  },
                                ),
                              ],
                            ),
                          ),
                        ],
                      );
                    },
                  );
                }
              },
            ),
          );
        }
        return Container();
      },
    );
  }
}
