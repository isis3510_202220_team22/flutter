import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tucompa/bloc/auth/auth_bloc.dart' hide Loading;
import 'package:tucompa/bloc/contact/contact_bloc.dart';
import 'package:tucompa/models/contact.dart';
import 'package:tucompa/views/add_contact_view.dart';

class ContactsView extends StatefulWidget {
  static const routeName = '/contacts';
  const ContactsView({Key? key}) : super(key: key);

  @override
  _ContactsViewState createState() => _ContactsViewState();
}

class _ContactsViewState extends State<ContactsView> {
  List<Contact> contacts = [
    // Contact(
    //   'Scarlett Johansson',
    //   '3225554577',
    //   "https://media.vanityfair.com/photos/5d2b5dfa9d463f0008d45c61/16:9/w_1280,c_limit/scarlett-johansson-play-tree.jpg",
    // ),
  ];
  bool loadedImage = true;

  Future<bool> _onWillPop(BuildContext ctx, int index, String user) async {
    FocusManager.instance.primaryFocus?.unfocus();
    return (await showDialog(
          context: ctx,
          builder: (ct) => AlertDialog(
            title: const Text('Eliminar'),
            content: const Text('¿Está seguro de eliminar el contacto?'),
            actions: <Widget>[
              TextButton(
                onPressed: () => Navigator.of(ctx).pop(false),
                child: const Text('No'),
              ),
              TextButton(
                onPressed: () {
                  Navigator.of(ctx).pop(true);
                  BlocProvider.of<ContactBloc>(context)
                      .add(DeleteContact(contacts[index], user));
                },
                child: const Text('Si'),
              ),
            ],
          ),
        )) ??
        false;
  }

  @override
  Widget build(BuildContext context) {
    String userEmail = (context.read<AuthBloc>().state as Authenticated).email;
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: Colors.transparent,
          title: const Text(
            'Contactos Favoritos',
            style: TextStyle(
              color: Color.fromRGBO(3, 69, 96, 1),
              fontWeight: FontWeight.bold,
              fontSize: 20,
            ),
          ),
          leading: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(5),
                child: CircleAvatar(
                  backgroundColor: const Color.fromRGBO(3, 69, 105, 1),
                  child: IconButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    icon: const Icon(
                      Icons.arrow_back,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        floatingActionButton: CircleAvatar(
          backgroundColor: const Color.fromRGBO(3, 69, 105, 1),
          radius: 30,
          child: IconButton(
            onPressed: () {
              Navigator.of(context).pushNamed(AddContactView.routeName);
            },
            color: Colors.white,
            icon: const Icon(Icons.add),
          ),
        ),
        body: BlocBuilder<ContactBloc, ContactState>(
            builder: (context, contactState) {
          if (contactState is Loading) {
            return const CircularProgressIndicator();
          } else if (contactState is Loaded) {
            contacts = contactState.contacts;
            if (contacts.isEmpty) {
              return Card(
                color: const Color.fromRGBO(216, 229, 237, 1),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: const [
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 10.0),
                      child: Text(
                        'No hay contactos registrados',
                        style: TextStyle(fontSize: 15),
                      ),
                    ),
                  ],
                ),
              );
            }
            return ListView.builder(
              itemBuilder: (context, index) => Card(
                key: ValueKey(DateTime.now()),
                color: const Color.fromRGBO(216, 229, 237, 1),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 8.0, horizontal: 20),
                      child: SizedBox(
                        height: 80.0,
                        width: 80.0,
                        child: CachedNetworkImage(
                          imageUrl: contacts[index].image,
                          placeholder: (context, url) =>
                              const CircularProgressIndicator(),
                          errorWidget: (context, url, error) => Stack(
                            children: [
                              Image.asset(
                                  'assets/images/profile-placeholder.png'),
                              Positioned(
                                top: 14,
                                left: 16,
                                child: IconButton(
                                  onPressed: () {
                                    setState(() {});
                                    ScaffoldMessenger.of(context)
                                        .clearSnackBars();
                                    ScaffoldMessenger.of(context).showSnackBar(
                                        const SnackBar(
                                            content: Text(
                                                'Intentando obtener la imagen')));
                                  },
                                  icon: const Icon(Icons.replay_outlined),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Column(
                      children: [
                        Text(
                          contacts[index].name,
                          style: const TextStyle(fontSize: 20),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Text(
                          'Cel: ${contacts[index].phoneNumber.toString()}',
                          style: const TextStyle(fontSize: 16),
                        ),
                      ],
                    ),
                    IconButton(
                      onPressed: () {
                        _onWillPop(context, index, userEmail);
                      },
                      iconSize: 35.0,
                      icon: const Icon(
                        Icons.delete_forever_outlined,
                        color: Colors.red,
                      ),
                    )
                  ],
                ),
              ),
              itemCount: contacts.length,
            );
          } else if (contactState is ContactError) {
            return Card(
              color: const Color.fromRGBO(216, 229, 237, 1),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10.0),
                    child: Text(
                      contactState.error,
                      style: const TextStyle(fontSize: 15),
                    ),
                  ),
                ],
              ),
            );
          }
          return Container();
        }),
      ),
    );
  }
}
