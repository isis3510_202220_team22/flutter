import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tucompa/widgets/rating_card.dart';
import '../bloc/auth/auth_bloc.dart';
import '../bloc/network/network_bloc.dart';
import '../bloc/route/route_bloc.dart';
import '../bloc/user_rating/user_rating_bloc.dart';

class RatingView extends StatefulWidget {
  final routeId;
  final email;

  RatingView({this.routeId, this.email});

  @override
  State<RatingView> createState() => _RatingViewState();
}

class _RatingViewState extends State<RatingView> {
  @override
  Widget build(BuildContext context) {
    final users = {};
    bool alone;

    String userEmail = '';
    if (context.read<AuthBloc>().state is Authenticated) {
      userEmail = context.read<AuthBloc>().state.props[0].toString();
    }

    void addRatings(double rate, String email) {
      users[email] = rate;
    }

    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              const Padding(
                padding: EdgeInsets.only(
                  top: 5,
                  left: 24,
                  bottom: 10,
                ),
                child: Text(
                  'Califica a tus acompañantes',
                  style: TextStyle(
                    color: Color.fromRGBO(3, 69, 96, 1),
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(left: 30),
                padding: const EdgeInsets.all(10),
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(7),
                  ),
                  color: Color.fromRGBO(216, 229, 237, 1),
                ),
                child: const Text(
                  'Usuarios a calificar',
                  style: TextStyle(
                    color: Color.fromRGBO(3, 69, 96, 1),
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: SizedBox(
                  height: MediaQuery.of(context).size.height * 0.7,
                  child: StreamBuilder(
                    key: ValueKey(DateTime.now()),
                    stream: BlocProvider.of<UserRatingBloc>(context)
                        .getUserRatings(widget.routeId),
                    builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
                      if (snapshot.data == null || snapshot.data!.size == 0) {
                        const CircleAvatar(
                          radius: 100,
                          child: CircularProgressIndicator(),
                        );
                      } else {
                        if (snapshot.data!.docs.length == 1) {
                          alone = true;
                          return Column(
                            children: [
                              BlocBuilder<NetworkBloc, NetworkState>(
                                builder: ((context, networkState) {
                                  if (networkState is NotConnectedState) {
                                    return Padding(
                                      padding: const EdgeInsets.only(left: 20),
                                      child: Container(
                                        height: MediaQuery.of(context)
                                                    .orientation ==
                                                Orientation.portrait
                                            ? MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.04
                                            : MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.07,
                                        width: MediaQuery.of(context)
                                                    .orientation ==
                                                Orientation.portrait
                                            ? MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.55
                                            : MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.3,
                                        decoration: const BoxDecoration(
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(7),
                                          ),
                                          color:
                                              Color.fromRGBO(216, 229, 237, 1),
                                        ),
                                        child: const Align(
                                          alignment: Alignment.center,
                                          child: Text(
                                            'No tiene conexión a internet!',
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              color:
                                                  Color.fromRGBO(3, 69, 96, 1),
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ),
                                      ),
                                    );
                                  }
                                  return Container();
                                }),
                              ),
                              const Padding(
                                padding: EdgeInsets.all(15),
                                child: Flexible(
                                  child: Text(
                                    'Parece que viajaste solo, no tienes que calificar a nadie',
                                    style: TextStyle(
                                      color: Color.fromRGBO(3, 69, 96, 1),
                                      fontSize: 18,
                                    ),
                                  ),
                                ),
                              ),
                              Align(
                                alignment: Alignment.bottomCenter,
                                child: RatingButton(
                                    userEmail, widget.routeId, alone, users),
                              )
                            ],
                          );
                        } else {
                          alone = false;
                          return Column(
                            children: [
                              BlocBuilder<NetworkBloc, NetworkState>(
                                builder: ((context, networkState) {
                                  if (networkState is NotConnectedState) {
                                    return Padding(
                                      padding: EdgeInsets.only(
                                          left: MediaQuery.of(context)
                                                      .orientation ==
                                                  Orientation.portrait
                                              ? 15
                                              : 20),
                                      child: Container(
                                        height: MediaQuery.of(context)
                                                    .orientation ==
                                                Orientation.portrait
                                            ? MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.04
                                            : MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.07,
                                        width: MediaQuery.of(context)
                                                    .orientation ==
                                                Orientation.portrait
                                            ? MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.55
                                            : MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.3,
                                        decoration: const BoxDecoration(
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(7),
                                          ),
                                          color:
                                              Color.fromRGBO(216, 229, 237, 1),
                                        ),
                                        child: const Align(
                                          alignment: Alignment.center,
                                          child: Text(
                                            'No tiene conexión a internet!',
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              color:
                                                  Color.fromRGBO(3, 69, 96, 1),
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ),
                                      ),
                                    );
                                  }
                                  return Container();
                                }),
                              ),
                              Column(
                                  children: snapshot.data!.docs.map((e) {
                                if (e.get('pending') == false &&
                                    widget.email != e.get('userEmail')) {
                                  return Padding(
                                    padding: EdgeInsets.only(
                                      left:
                                          MediaQuery.of(context).orientation ==
                                                  Orientation.portrait
                                              ? 0
                                              : 20,
                                      top: 15,
                                    ),
                                    child: RatingCard(e.get('userEmail'),
                                        userEmail, addRatings),
                                  );
                                } else {
                                  return Container();
                                }
                              }).toList()),
                              RatingButton(
                                  userEmail, widget.routeId, alone, users)
                            ],
                          );
                        }
                      }
                      return Container();
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class RatingButton extends StatelessWidget {
  final String userEmail;
  final String routeId;
  final bool alone;
  final Map<dynamic, dynamic> users;

  RatingButton(this.userEmail, this.routeId, this.alone, this.users);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: BlocBuilder<NetworkBloc, NetworkState>(
        builder: ((context, networkState) {
          if (networkState is NotConnectedState) {
            return InkWell(
              onTap: () {
                ScaffoldMessenger.of(context).clearSnackBars();
                ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(
                    content: Text(
                        'Debe tener conexión a internet para poder enviar las calificaiones o terminar el viaje'),
                  ),
                );
              },
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  height:
                      MediaQuery.of(context).orientation == Orientation.portrait
                          ? MediaQuery.of(context).size.height * 0.04
                          : MediaQuery.of(context).size.height * 0.09,
                  width: MediaQuery.of(context).size.width * 0.5,
                  decoration: const BoxDecoration(
                    color: Color.fromRGBO(216, 229, 237, 1),
                    borderRadius: BorderRadius.all(
                      Radius.circular(9),
                    ),
                  ),
                  child: Align(
                    alignment: Alignment.center,
                    child: Text(
                      alone ? 'TERMINAR VIAJE' : 'ENVIAR CALIFICACIONES',
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Color.fromRGBO(3, 69, 96, 1),
                      ),
                    ),
                  ),
                ),
              ),
            );
          }
          return InkWell(
            onTap: () {
              showDialog<void>(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: const [
                        Text('Calificaciones'),
                        SizedBox(
                          width: 10,
                        ),
                        Icon(Icons.star),
                      ],
                    ),
                    content: Text(
                      alone
                          ? 'Ya finalizó el viaje'
                          : 'Ya se enviaron sus calificaciones',
                    ),
                    actions: <Widget>[
                      Center(
                        child: TextButton(
                          child: const Text('Ok'),
                          onPressed: () {
                            BlocProvider.of<UserRatingBloc>(context)
                                .add(SendRatings(users, routeId, userEmail));
                            BlocProvider.of<RouteBloc>(context)
                                .add(ExitRoute(routeId, userEmail));
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                    ],
                  );
                },
              );
            },
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                height:
                    MediaQuery.of(context).orientation == Orientation.portrait
                        ? MediaQuery.of(context).size.height * 0.04
                        : MediaQuery.of(context).size.height * 0.09,
                width: MediaQuery.of(context).size.width * 0.5,
                decoration: const BoxDecoration(
                  color: Color.fromRGBO(216, 229, 237, 1),
                  borderRadius: BorderRadius.all(
                    Radius.circular(9),
                  ),
                ),
                child: Align(
                  alignment: Alignment.center,
                  child: Text(
                    alone ? 'TERMINAR VIAJE' : 'ENVIAR CALIFICACIONES',
                    style: const TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Color.fromRGBO(3, 69, 96, 1),
                    ),
                  ),
                ),
              ),
            ),
          );
        }),
      ),
    );
  }
}
