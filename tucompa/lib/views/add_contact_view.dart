import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tucompa/bloc/auth/auth_bloc.dart';
import 'package:tucompa/bloc/contact/contact_bloc.dart';
import 'package:tucompa/models/contact.dart';

class AddContactView extends StatefulWidget {
  static const routeName = '/add-contacts';
  const AddContactView({Key? key}) : super(key: key);

  @override
  _AddContactViewState createState() => _AddContactViewState();
}

class _AddContactViewState extends State<AddContactView> {
  int? selectedIcon;

  TextEditingController nameController = TextEditingController();
  TextEditingController numberController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  List<String> images = [
    'https://cdn-icons-png.flaticon.com/512/4441/4441180.png',
    'https://cdn-icons-png.flaticon.com/512/2024/2024101.png',
    'https://cdn-icons-png.flaticon.com/512/3074/3074318.png'
  ];

  @override
  void dispose() {
    nameController.dispose();
    numberController.dispose();
    super.dispose();
  }

  void showSnackBar(BuildContext ctx, String message) {
    ScaffoldMessenger.of(ctx).clearSnackBars();
    ScaffoldMessenger.of(ctx).showSnackBar(SnackBar(content: Text(message)));
  }

  void _trySubmit(BuildContext ctx, String user) {
    FocusScope.of(context).unfocus();

    if (nameController.text == '') {
      showSnackBar(ctx, 'Debe ingresar un nombre');
    } else if (numberController.text.trim() == '') {
      showSnackBar(ctx, 'Debe ingresar un número');
    } else if (int.tryParse(numberController.text.trim()) == null) {
      showSnackBar(ctx, 'Debe ingresar un número válido');
    } else if (selectedIcon == null) {
      showSnackBar(ctx, 'Debe seleccionar el tipo de contacto');
    } else {
      BlocProvider.of<ContactBloc>(ctx).add(
        SaveContact(
            Contact(
              nameController.text,
              numberController.text,
              images[selectedIcon!],
            ),
            user),
      );
      Navigator.of(ctx).pop();
    }
  }

  @override
  Widget build(BuildContext context) {
    final FirebaseAnalytics analytics = FirebaseAnalytics.instance;

    Future<void> sendAnalyticsEvent(String eventName) async {
      await analytics.logEvent(name: eventName);
    }

    String userEmail = (context.read<AuthBloc>().state as Authenticated).email;
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: SafeArea(
        child: Scaffold(
          appBar: AppBar(
            elevation: 0.0,
            backgroundColor: Colors.transparent,
            title: const Text(
              'Agregar Contacto',
              style: TextStyle(
                color: Color.fromRGBO(3, 69, 96, 1),
                fontWeight: FontWeight.bold,
                fontSize: 20,
              ),
            ),
            leading: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(5),
                  child: CircleAvatar(
                    backgroundColor: const Color.fromRGBO(3, 69, 105, 1),
                    child: IconButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      icon: const Icon(
                        Icons.arrow_back,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          body: SingleChildScrollView(
            child: Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    TextFormField(
                      controller: nameController,
                      maxLength: 15,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: const Color.fromRGBO(242, 244, 245, 1),
                        border: OutlineInputBorder(
                          borderSide: BorderSide.none,
                          borderRadius: BorderRadius.circular(15),
                        ),
                        contentPadding: const EdgeInsets.symmetric(
                          horizontal: 10,
                          vertical: 10,
                        ),
                        labelText: 'Nombre',
                      ),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    TextFormField(
                      controller: numberController,
                      keyboardType: TextInputType.phone,
                      maxLength: 10,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: const Color.fromRGBO(242, 244, 245, 1),
                        border: OutlineInputBorder(
                          borderSide: BorderSide.none,
                          borderRadius: BorderRadius.circular(15),
                        ),
                        contentPadding: const EdgeInsets.symmetric(
                          horizontal: 10,
                          vertical: 10,
                        ),
                        labelText: 'Número',
                      ),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Column(
                          children: [
                            IconButton(
                              iconSize:
                                  selectedIcon != null && selectedIcon == 0
                                      ? 70
                                      : 40,
                              icon: CachedNetworkImage(
                                imageUrl: images[0],
                                placeholder: (context, url) =>
                                    const CircularProgressIndicator(),
                                errorWidget: (context, url, error) =>
                                    Image.asset(
                                        'assets/images/profile-placeholder.png'),
                              ),
                              onPressed: () {
                                setState(() {
                                  selectedIcon = 0;
                                });
                                FocusManager.instance.primaryFocus?.unfocus();
                              },
                            ),
                            Text(
                              'Familia',
                              style: TextStyle(
                                  decoration:
                                      selectedIcon != null && selectedIcon == 0
                                          ? TextDecoration.underline
                                          : TextDecoration.none),
                            ),
                          ],
                        ),
                        Column(
                          children: [
                            IconButton(
                              iconSize:
                                  selectedIcon != null && selectedIcon == 1
                                      ? 70
                                      : 40,
                              icon: CachedNetworkImage(
                                imageUrl: images[1],
                                placeholder: (context, url) =>
                                    const CircularProgressIndicator(),
                                errorWidget: (context, url, error) =>
                                    Image.asset(
                                        'assets/images/profile-placeholder.png'),
                              ),
                              onPressed: () {
                                setState(() {
                                  selectedIcon = 1;
                                });
                                FocusManager.instance.primaryFocus?.unfocus();
                              },
                            ),
                            Text(
                              'Amigx',
                              style: TextStyle(
                                  decoration:
                                      selectedIcon != null && selectedIcon == 1
                                          ? TextDecoration.underline
                                          : TextDecoration.none),
                            ),
                          ],
                        ),
                        Column(
                          children: [
                            IconButton(
                              iconSize:
                                  selectedIcon != null && selectedIcon == 2
                                      ? 70
                                      : 40,
                              icon: CachedNetworkImage(
                                imageUrl: images[2],
                                placeholder: (context, url) =>
                                    const CircularProgressIndicator(),
                                errorWidget: (context, url, error) =>
                                    Image.asset(
                                        'assets/images/profile-placeholder.png'),
                              ),
                              onPressed: () {
                                setState(() {
                                  selectedIcon = 2;
                                });
                                FocusManager.instance.primaryFocus?.unfocus();
                              },
                            ),
                            Text(
                              'Pareja',
                              style: TextStyle(
                                  decoration:
                                      selectedIcon != null && selectedIcon == 2
                                          ? TextDecoration.underline
                                          : TextDecoration.none),
                            ),
                          ],
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    TextButton(
                      onPressed: () {
                        _trySubmit(context, userEmail);
                        sendAnalyticsEvent('add_contact');
                      },
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(
                          const Color.fromRGBO(3, 69, 96, 1),
                        ),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20),
                          ),
                        ),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10.0),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: const [
                            Text(
                              'Agregar',
                              style: TextStyle(color: Colors.white),
                            ),
                            Icon(Icons.arrow_forward, color: Colors.white),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
