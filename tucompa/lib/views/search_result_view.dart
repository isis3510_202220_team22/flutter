import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tucompa/bloc/user/user_bloc.dart' as user_bloc;
import 'package:tucompa/bloc/user_routes/user_routes_bloc.dart';
import 'package:tucompa/bloc/network/network_bloc.dart';
import 'package:tucompa/widgets/result_card.dart';

import '../bloc/user/user_bloc.dart';

class SearchResultView extends StatefulWidget {
  static const routeName = '/search-result';
  bool userHasRoute = false;
  SearchResultView({super.key});

  @override
  State<SearchResultView> createState() => _SearchResultViewState();
}

class _SearchResultViewState extends State<SearchResultView> {
  @override
  Widget build(BuildContext context) {
    user_bloc.UserState state =
        BlocProvider.of<user_bloc.UserBloc>(context).state;
    String userEmail = state is user_bloc.FetchUser ? state.email : '';
    return SafeArea(
      child: RefreshIndicator(
        onRefresh: () async {
          NetworkState networkState = context.read<NetworkBloc>().state;
          if (networkState is! ConnectedState) {
            ScaffoldMessenger.of(context).clearSnackBars();
            ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
              content: Text(
                  'No puede actualizar sin conexión a internet. Intente de nuevo más tarde.'),
            ));
          } else {
            UserRoutesState userState = context.read<UserRoutesBloc>().state;
            if (userState is UserRoutesError) {
              BlocProvider.of<UserRoutesBloc>(context).add(FetchRoutes(
                  userState.origin!,
                  userState.destination!,
                  userState.dateTime!));
            }
            if (userState is FetchComplete) {
              BlocProvider.of<UserRoutesBloc>(context).add(FetchRoutes(
                  userState.origin, userState.destination, userState.dateTime));
            }
          }
        },
        child: Scaffold(
          appBar: AppBar(
            elevation: 0.0,
            backgroundColor: Colors.transparent,
            leading: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(5),
                  child: CircleAvatar(
                    backgroundColor: const Color.fromRGBO(3, 69, 105, 1),
                    child: IconButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                        BlocProvider.of<UserRoutesBloc>(context)
                            .add(ExitView());
                      },
                      icon: const Icon(
                        Icons.arrow_back,
                        color: Colors.white,
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
          body: BlocBuilder<UserRoutesBloc, UserRoutesState>(
            builder: (context, state) {
              if (state is FetchComplete) {
                if (state.routes.isEmpty) {
                  return ListView(
                    children: const [
                      Card(
                        color: Color.fromRGBO(216, 229, 237, 1),
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                            vertical: 20,
                            horizontal: 8.0,
                          ),
                          child: Text(
                            "No hay rutas con los filtros seleccionados",
                            softWrap: true,
                            style: TextStyle(fontSize: 18),
                          ),
                        ),
                      ),
                    ],
                  );
                } else {
                  for (int i = 0; i < state.routes.length; i++) {
                    if (state.routes[i].userEmail == userEmail) {
                      widget.userHasRoute = true;
                    }
                  }
                }
                return StreamBuilder(
                  stream:
                      BlocProvider.of<UserBloc>(context).getUserInfo(userEmail),
                  builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
                    if (snapshot.data != null) {
                      var isInRoute = false;
                      if (snapshot.data!.docs[0].get('isInRoute') == true) {
                        isInRoute = true;
                      }
                      return ListView.builder(
                        itemBuilder: (context, index) {
                          return ResultCard(
                            state.users[index],
                            state.routes[index],
                            widget.userHasRoute,
                            isInRoute,
                            (isInRoute == true &&
                                state.routes[index].id ==
                                    snapshot.data!.docs[0].get('currentRoute')),
                            userEmail,
                            key: ValueKey(
                              DateTime.now(),
                            ),
                          );
                        },
                        itemCount: state.routes.length,
                      );
                    }
                    return Container();
                  },
                );
              }
              if (state is UserRoutesError) {
                return ListView(
                  children: [
                    Card(
                      color: const Color.fromRGBO(216, 229, 237, 1),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                          vertical: 20,
                          horizontal: 8.0,
                        ),
                        child: Text(
                          state.error,
                          softWrap: true,
                          style: const TextStyle(fontSize: 18),
                        ),
                      ),
                    ),
                  ],
                );
              }
              if (state is UserRoutesLoading) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }
              return Container();
            },
          ),
        ),
      ),
    );
  }
}
