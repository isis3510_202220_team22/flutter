import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tucompa/bloc/user_rating/user_rating_bloc.dart';
import 'package:tucompa/widgets/requests_card.dart';
import '../bloc/auth/auth_bloc.dart';
import '../bloc/network/network_bloc.dart';
import '../bloc/route/route_bloc.dart';
import 'dart:async';

import '../bloc/user/user_bloc.dart';

class CurrentCommuteView extends StatefulWidget {
  @override
  State<CurrentCommuteView> createState() => _CurrentCommuteViewState();
}

class _CurrentCommuteViewState extends State<CurrentCommuteView> {
  String message = 'ELIMINAR VIAJE';
  @override
  Widget build(BuildContext context) {
    String routeId = '';
    String hour = '';
    String origin = '';
    String destination = '';
    String ownerEmail = '';

    final state = BlocProvider.of<RouteBloc>(context).state;

    String userEmail = '';
    if (context.read<AuthBloc>().state is Authenticated) {
      userEmail = context.read<AuthBloc>().state.props[0].toString();
    }

    if (state is CurrentRoute) {
      routeId = state.routeData[0];
      hour = state.routeData[1];
      origin = state.routeData[2];
      destination = state.routeData[3];
      ownerEmail = state.routeData[4];

      Timer.periodic(const Duration(seconds: 10), (Timer t) {
        if (mounted) {
          if (hour.compareTo(DateTime.now().toString()) < 0) {
            if (message != 'FINALIZAR VIAJE') {
              setState(() {
                message = 'FINALIZAR VIAJE';
              });
            }
            t.cancel();
          }
        }
      });
    }

    void acceptUser(String routeId, String userEmail) {
      BlocProvider.of<UserRatingBloc>(context)
          .add(AcceptUser(routeId, userEmail));
    }

    void denyUser(String routeId, String userEmail) {
      BlocProvider.of<UserRatingBloc>(context)
          .add(DeleteUser(routeId, userEmail));
    }

    void messageUser(String routeId, String userEmail) {
      showDialog<void>(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Row(
                mainAxisSize: MainAxisSize.min,
                children: const [
                  Text('Espera'),
                  SizedBox(
                    width: 10,
                  ),
                  Icon(Icons.timer),
                ],
              ),
              content: const Text('Esta función aún no está disponible'),
              actions: <Widget>[
                Center(
                  child: TextButton(
                    child: const Text('Ok'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ),
              ],
            );
          });
    }

    return SafeArea(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            const Padding(
              padding: EdgeInsets.only(
                top: 5,
                left: 24,
              ),
              child: Text(
                'Información del viaje',
                style: TextStyle(
                  color: Color.fromRGBO(3, 69, 96, 1),
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                ),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(left: 20, top: 3),
              height: MediaQuery.of(context).orientation == Orientation.portrait
                  ? MediaQuery.of(context).size.height * 0.17
                  : MediaQuery.of(context).size.height * 0.25,
              width: MediaQuery.of(context).orientation == Orientation.portrait
                  ? MediaQuery.of(context).size.width * 0.9
                  : MediaQuery.of(context).size.width * 0.65,
              decoration: const BoxDecoration(
                color: Color.fromRGBO(3, 69, 96, 1),
                borderRadius: BorderRadius.all(
                  Radius.circular(20),
                ),
              ),
              child: SingleChildScrollView(
                child: Flex(
                  direction:
                      MediaQuery.of(context).orientation == Orientation.portrait
                          ? Axis.vertical
                          : Axis.horizontal,
                  children: <Widget>[
                    Flex(
                      direction: MediaQuery.of(context).orientation ==
                              Orientation.portrait
                          ? Axis.horizontal
                          : Axis.vertical,
                      children: <Widget>[
                        Container(
                          margin: const EdgeInsets.only(left: 20, top: 15),
                          padding: const EdgeInsets.all(5),
                          decoration: const BoxDecoration(
                            borderRadius: BorderRadius.all(
                              Radius.circular(15),
                            ),
                            color: Color.fromRGBO(216, 229, 237, 1),
                          ),
                          child: const Text(
                            'Hora de salida:',
                            style: TextStyle(
                              color: Color.fromRGBO(3, 69, 96, 1),
                              fontSize: 15,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                            top: 15,
                            left: MediaQuery.of(context).orientation ==
                                    Orientation.portrait
                                ? 75
                                : 7,
                          ),
                          child: Text(
                            hour.substring(10, 16),
                            style: const TextStyle(
                              color: Color.fromRGBO(216, 229, 237, 1),
                              fontSize: 17,
                            ),
                          ),
                        )
                      ],
                    ),
                    Flex(
                      direction: MediaQuery.of(context).orientation ==
                              Orientation.portrait
                          ? Axis.horizontal
                          : Axis.vertical,
                      children: [
                        Container(
                          margin: const EdgeInsets.only(left: 20, top: 15),
                          padding: const EdgeInsets.all(5),
                          decoration: const BoxDecoration(
                            borderRadius: BorderRadius.all(
                              Radius.circular(15),
                            ),
                            color: Color.fromRGBO(216, 229, 237, 1),
                          ),
                          child: const Text(
                            'Estación de partida:',
                            style: TextStyle(
                              color: Color.fromRGBO(3, 69, 96, 1),
                              fontSize: 15,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 15, left: 25),
                          child: Text(
                            origin,
                            style: const TextStyle(
                              color: Color.fromRGBO(216, 229, 237, 1),
                              fontSize: 16,
                            ),
                          ),
                        )
                      ],
                    ),
                    Flex(
                      direction: MediaQuery.of(context).orientation ==
                              Orientation.portrait
                          ? Axis.horizontal
                          : Axis.vertical,
                      children: [
                        Container(
                          margin: const EdgeInsets.only(left: 20, top: 15),
                          padding: const EdgeInsets.all(5),
                          decoration: const BoxDecoration(
                            borderRadius: BorderRadius.all(
                              Radius.circular(15),
                            ),
                            color: Color.fromRGBO(216, 229, 237, 1),
                          ),
                          child: const Text(
                            'Estación de destino:',
                            style: TextStyle(
                              color: Color.fromRGBO(3, 69, 96, 1),
                              fontSize: 15,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 25, top: 15),
                          child: Text(
                            destination,
                            style: const TextStyle(
                              color: Color.fromRGBO(216, 229, 237, 1),
                              fontSize: 15,
                            ),
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
            ownerEmail == userEmail
                ? Container(
                    padding: const EdgeInsets.all(10),
                    height: MediaQuery.of(context).orientation ==
                            Orientation.portrait
                        ? MediaQuery.of(context).size.height * 0.31
                        : MediaQuery.of(context).size.height * 0.5,
                    width: MediaQuery.of(context).size.width * 0.9,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        BlocBuilder<NetworkBloc, NetworkState>(
                          builder: ((context, networkState) {
                            if (networkState is NotConnectedState) {
                              return Padding(
                                padding: EdgeInsets.only(
                                    left: MediaQuery.of(context).orientation ==
                                            Orientation.portrait
                                        ? 80
                                        : 150),
                                child: Container(
                                  height: MediaQuery.of(context).orientation ==
                                          Orientation.portrait
                                      ? MediaQuery.of(context).size.height *
                                          0.04
                                      : MediaQuery.of(context).size.height *
                                          0.07,
                                  width: MediaQuery.of(context).orientation ==
                                          Orientation.portrait
                                      ? MediaQuery.of(context).size.width * 0.55
                                      : MediaQuery.of(context).size.width * 0.3,
                                  decoration: const BoxDecoration(
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(7),
                                    ),
                                    color: Color.fromRGBO(216, 229, 237, 1),
                                  ),
                                  child: const Align(
                                    alignment: Alignment.center,
                                    child: Text(
                                      'No tiene conexión a internet!',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        color: Color.fromRGBO(3, 69, 96, 1),
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            }
                            return Container();
                          }),
                        ),
                        message == 'ELIMINAR VIAJE'
                            ? const Text(
                                'Solicitudes pendientes',
                                style: TextStyle(
                                  color: Color.fromRGBO(3, 69, 96, 1),
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                ),
                              )
                            : Container(),
                        message == 'ELIMINAR VIAJE'
                            ? Expanded(
                                child: SingleChildScrollView(
                                  child: StreamBuilder(
                                    key: ValueKey(DateTime.now()),
                                    stream:
                                        BlocProvider.of<UserRatingBloc>(context)
                                            .getUserRatings(routeId),
                                    builder: (context,
                                        AsyncSnapshot<QuerySnapshot> snapshot) {
                                      if (snapshot.data == null ||
                                          snapshot.data!.size == 0) {
                                        const CircleAvatar(
                                          radius: 100,
                                          child: CircularProgressIndicator(),
                                        );
                                      } else {
                                        return Column(
                                            children:
                                                snapshot.data!.docs.map((e) {
                                          if (e.get('pending') == true) {
                                            return StreamBuilder(
                                              stream: BlocProvider.of<UserBloc>(
                                                      context)
                                                  .getUserInfo(
                                                      e.get('userEmail')),
                                              builder: (context,
                                                  AsyncSnapshot<QuerySnapshot>
                                                      snapshot) {
                                                if (snapshot.data != null) {
                                                  if (snapshot.data!.docs[0]
                                                          .get('isInRoute') ==
                                                      false) {
                                                    return Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                        left: 40,
                                                        top: 15,
                                                      ),
                                                      child: RequestsCard(
                                                          e.get('userEmail'),
                                                          e.get('pending'),
                                                          routeId,
                                                          ownerEmail ==
                                                              userEmail,
                                                          ownerEmail,
                                                          acceptUser,
                                                          denyUser),
                                                    );
                                                  }
                                                  return Container();
                                                }
                                                return Container();
                                              },
                                            );
                                          } else {
                                            return Container();
                                          }
                                        }).toList());
                                      }
                                      return Container();
                                    },
                                  ),
                                ),
                              )
                            : Container(),
                      ],
                    ),
                  )
                : Container(),
            Container(
              padding: const EdgeInsets.all(10),
              height: ownerEmail == userEmail
                  ? MediaQuery.of(context).orientation == Orientation.portrait
                      ? MediaQuery.of(context).size.height * 0.31
                      : MediaQuery.of(context).size.height * 0.5
                  : MediaQuery.of(context).size.height * 0.62,
              width: MediaQuery.of(context).size.width * 0.9,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  const Text(
                    'Usuarios del viaje',
                    style: TextStyle(
                      color: Color.fromRGBO(3, 69, 96, 1),
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Expanded(
                    child: SingleChildScrollView(
                      child: StreamBuilder(
                        key: ValueKey(DateTime.now()),
                        stream: BlocProvider.of<UserRatingBloc>(context)
                            .getUserRatings(routeId),
                        builder:
                            (context, AsyncSnapshot<QuerySnapshot> snapshot) {
                          if (snapshot.data == null ||
                              snapshot.data!.size == 0) {
                            const CircleAvatar(
                              radius: 100,
                              child: CircularProgressIndicator(),
                            );
                          } else {
                            return Column(
                                children: snapshot.data!.docs.map((e) {
                              if (e.get('pending') == false) {
                                return Padding(
                                  padding: const EdgeInsets.only(
                                    left: 40,
                                    top: 15,
                                  ),
                                  child: RequestsCard(
                                      e.get('userEmail'),
                                      e.get('pending'),
                                      routeId,
                                      ownerEmail == userEmail,
                                      ownerEmail,
                                      messageUser,
                                      denyUser),
                                );
                              } else {
                                return Container();
                              }
                            }).toList());
                          }
                          return Container();
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
            ownerEmail == userEmail
                ? Padding(
                    padding: MediaQuery.of(context).orientation ==
                            Orientation.portrait
                        ? const EdgeInsets.only(bottom: 0)
                        : const EdgeInsets.only(bottom: 10),
                    child: Align(
                      alignment: Alignment.center,
                      child: BlocBuilder<NetworkBloc, NetworkState>(
                        builder: ((context, networkState) {
                          if (networkState is NotConnectedState) {
                            return InkWell(
                              onTap: () {
                                ScaffoldMessenger.of(context).clearSnackBars();
                                ScaffoldMessenger.of(context).showSnackBar(
                                  const SnackBar(
                                    content: Text(
                                        'Debe tener conexión a internet para poder eliminar o finalizar un viaje '),
                                  ),
                                );
                              },
                              child: Container(
                                height: MediaQuery.of(context).orientation ==
                                        Orientation.portrait
                                    ? MediaQuery.of(context).size.height * 0.04
                                    : MediaQuery.of(context).size.height * 0.09,
                                width: MediaQuery.of(context).size.width * 0.35,
                                decoration: const BoxDecoration(
                                  color: Color.fromRGBO(216, 229, 237, 1),
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(9),
                                  ),
                                ),
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Text(
                                    hour.compareTo(DateTime.now().toString()) >
                                            0
                                        ? 'ELIMINAR VIAJE'
                                        : 'FINALIZAR VIAJE',
                                    style: const TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Color.fromRGBO(3, 69, 96, 1),
                                    ),
                                  ),
                                ),
                              ),
                            );
                          }
                          return InkWell(
                            onTap: () {
                              message == 'ELIMINAR VIAJE'
                                  ? BlocProvider.of<RouteBloc>(context)
                                      .add(DeleteRoute(routeId))
                                  : BlocProvider.of<RouteBloc>(context)
                                      .add(RateRoute(routeId));
                            },
                            child: Container(
                              height: MediaQuery.of(context).orientation ==
                                      Orientation.portrait
                                  ? MediaQuery.of(context).size.height * 0.04
                                  : MediaQuery.of(context).size.height * 0.09,
                              width: MediaQuery.of(context).size.width * 0.35,
                              decoration: const BoxDecoration(
                                color: Color.fromRGBO(216, 229, 237, 1),
                                borderRadius: BorderRadius.all(
                                  Radius.circular(9),
                                ),
                              ),
                              child: Align(
                                alignment: Alignment.center,
                                child: Text(
                                  message,
                                  style: const TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Color.fromRGBO(3, 69, 96, 1),
                                  ),
                                ),
                              ),
                            ),
                          );
                        }),
                      ),
                    ),
                  )
                : Container(),
          ],
        ),
      ),
    );
  }
}
