import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tucompa/views/profile_view.dart';
import 'package:tucompa/views/publish_commute_view.dart';
import 'package:tucompa/views/search_view.dart';
import '../bloc/route/route_bloc.dart';
import '../bloc/station/station_bloc.dart';

class NavigationView extends StatefulWidget {
  static const routeName = '/nav';
  @override
  State<NavigationView> createState() => _NavigationViewState();
}

class _NavigationViewState extends State<NavigationView> {
  int _selectedIndex = 1;
  String userEmail = '';

  final List<Widget> _screens = [
    const SearchView(),
    PublishCommuteView(),
    const ProfileView(),
  ];

  void _onItemTapped(int index) {
    if (index != 2) {
      BlocProvider.of<StationBloc>(context).add(InitializeStations());
      BlocProvider.of<StationBloc>(context).add(GetStations());
    } else {
      BlocProvider.of<StationBloc>(context).add(InitializeStations());
    }

    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        backgroundColor: const Color.fromRGBO(3, 69, 96, 1),
        selectedItemColor: Colors.white,
        selectedIconTheme: const IconThemeData(size: 35),
        unselectedItemColor: Colors.white,
        showSelectedLabels: false,
        showUnselectedLabels: false,
        currentIndex: _selectedIndex, //New
        onTap: _onItemTapped,
        items: const [
          BottomNavigationBarItem(icon: Icon(Icons.search_sharp), label: ''),
          BottomNavigationBarItem(icon: Icon(Icons.map_outlined), label: ''),
          BottomNavigationBarItem(
              icon: Icon(Icons.account_circle_outlined), label: ''),
        ],
      ),
      body: _screens[_selectedIndex],
    );
  }
}
