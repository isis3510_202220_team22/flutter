import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:permission_handler/permission_handler.dart';
import '../bloc/network/network_bloc.dart';
import '../bloc/station/station_bloc.dart';

class QrView extends StatefulWidget {
  static const routeName = '/qr';

  @override
  State<QrView> createState() => _QrViewState();
}

class _QrViewState extends State<QrView> with WidgetsBindingObserver {
  String description = '';
  Barcode? result;
  String res = '';
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  QRViewController? controller;
  bool _status = false;

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    Permission.camera.isGranted.then((value) {
      if (value == true) {
        setState(() {
          _status = true;
        });
      } else {
        setState(() {
          _status = false;
        });
      }
    });
    super.initState();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    if (state == AppLifecycleState.resumed) {
      Permission.camera.isGranted.then((value) {
        if (value == true) {
          setState(() {
            _status = true;
          });
        } else {
          setState(() {
            _status = false;
          });
        }
      });
    }
  }

  Widget _buildQrView(BuildContext conext) {
    return QRView(
      key: qrKey,
      onQRViewCreated: _onQRViewCreated,
      onPermissionSet: (p0, p1) {
        print(p1);
        if (p1 == true) {
          setState(() {});
        } else {
          setState(() {});
        }
      },
      overlay: QrScannerOverlayShape(
        borderColor: const Color.fromRGBO(132, 168, 184, 1),
        borderRadius: 30,
        borderLength: 30,
        borderWidth: 10,
      ),
    );
  }

  void _onQRViewCreated(QRViewController controller) async {
    setState(() {
      this.controller = controller;
    });

    controller.resumeCamera();
    controller.scannedDataStream.listen(
      (scanData) {
        setState(() {
          result = scanData;
          res = result!.code.toString();
        });

        if (result != null) {
          BlocProvider.of<StationBloc>(context).add(
              GetWagonInfo(res.split('-')[0].trim(), res.split('-')[1].trim()));
        }
      },
    );
  }

  void _flipCamera() {
    controller?.flipCamera();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<StationBloc, StationState>(
      listener: ((context, state) {
        if (state is WagonInformation) {
          setState(() {
            description = state.description;
          });
        }
      }),
      child: Scaffold(
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: Colors.transparent,
          leading: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(5),
                child: CircleAvatar(
                  backgroundColor: const Color.fromRGBO(3, 69, 105, 1),
                  child: IconButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    icon: const Icon(
                      Icons.arrow_back,
                      color: Colors.white,
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
        body: OrientationBuilder(
          builder: (context, orientation) {
            return GridView.count(
              childAspectRatio: orientation == Orientation.portrait
                  ? MediaQuery.of(context).size.width /
                      ((MediaQuery.of(context).size.height) / 1.9)
                  : MediaQuery.of(context).size.width /
                      (MediaQuery.of(context).size.height * 1.6),
              crossAxisCount: orientation == Orientation.portrait ? 1 : 2,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    SizedBox(
                      width: MediaQuery.of(context).size.width,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          ClipRRect(
                            borderRadius: const BorderRadius.all(
                              Radius.circular(30),
                            ),
                            child: Container(
                              height: MediaQuery.of(context).orientation ==
                                      Orientation.portrait
                                  ? ((MediaQuery.of(context).size.height -
                                          MediaQuery.of(context)
                                              .viewPadding
                                              .top) *
                                      0.35)
                                  : (MediaQuery.of(context).size.height -
                                          MediaQuery.of(context)
                                              .viewPadding
                                              .top) *
                                      0.6,
                              width: MediaQuery.of(context).orientation ==
                                      Orientation.portrait
                                  ? MediaQuery.of(context).size.width * 0.9
                                  : MediaQuery.of(context).size.width * 0.8,
                              color: const Color.fromRGBO(217, 217, 217, 1),
                              child: _buildQrView(context),
                            ),
                          ),
                          SizedBox(
                            height: (MediaQuery.of(context).size.height -
                                    MediaQuery.of(context).viewPadding.top) *
                                0.05,
                          ),
                          ClipRRect(
                            borderRadius: const BorderRadius.all(
                              Radius.circular(30),
                            ),
                            child: InkWell(
                              onTap: _flipCamera,
                              child: Container(
                                height:
                                    MediaQuery.of(context).size.height * 0.06,
                                width: MediaQuery.of(context).size.width * 0.4,
                                decoration: const BoxDecoration(
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(7),
                                  ),
                                  color: Color.fromRGBO(216, 229, 237, 1),
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: const <Widget>[
                                    Text(
                                      'Cambiar cámara ',
                                      style: TextStyle(
                                        color: Color.fromRGBO(3, 69, 96, 1),
                                        fontWeight: FontWeight.bold,
                                        fontSize: 15,
                                      ),
                                    ),
                                    Icon(
                                      Icons.camera_alt_rounded,
                                      color: Color.fromRGBO(3, 69, 96, 1),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Container(
                    width: MediaQuery.of(context).size.width,
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(20),
                      ),
                      color: Color.fromRGBO(216, 229, 237, 1),
                    ),
                    child: BlocBuilder<NetworkBloc, NetworkState>(
                      builder: ((context, networkState) {
                        if (networkState is NotConnectedState) {
                          controller!.pauseCamera();
                          return Column(
                            children: const [
                              SizedBox(
                                height: 20,
                              ),
                              Text(
                                'No tiene conexión a internet!',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Color.fromRGBO(3, 69, 105, 1),
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20,
                                ),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Text(
                                'Debe tener conexión a internet para poder escanear un código QR',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Color.fromRGBO(3, 69, 105, 1),
                                  fontSize: 17,
                                ),
                              )
                            ],
                          );
                        }
                        controller!.resumeCamera();
                        return BlocBuilder<StationBloc, StationState>(
                          builder: (context, state) {
                            if (state is StationInitial ||
                                state is LoadedStations) {
                              return Padding(
                                padding: const EdgeInsets.all(25),
                                child: Column(
                                  children: <Widget>[
                                    Text(
                                      _status
                                          ? 'Escanee un QR'
                                          : 'No ha aceptado los permisos de la cámara',
                                      textAlign: TextAlign.center,
                                      style: const TextStyle(
                                        color: Color.fromRGBO(3, 69, 105, 1),
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20,
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 50,
                                    ),
                                    _status
                                        ? Container()
                                        : InkWell(
                                            onTap: () {
                                              openAppSettings();
                                            },
                                            child: Container(
                                              height: MediaQuery.of(context)
                                                          .orientation ==
                                                      Orientation.portrait
                                                  ? MediaQuery.of(context)
                                                          .size
                                                          .height *
                                                      0.12
                                                  : MediaQuery.of(context)
                                                          .size
                                                          .height *
                                                      0.22,
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  0.53,
                                              decoration: const BoxDecoration(
                                                borderRadius: BorderRadius.all(
                                                  Radius.circular(20),
                                                ),
                                                color: Color.fromRGBO(
                                                    3, 69, 105, 1),
                                              ),
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(10),
                                                child: Align(
                                                  alignment: Alignment.center,
                                                  child: Column(
                                                    children: const [
                                                      Text(
                                                        'Presione aqui para abrir las configuraciones de permisos de su dispositivo',
                                                        style: TextStyle(
                                                          color: Colors.white,
                                                          fontSize: 15,
                                                        ),
                                                      ),
                                                      Icon(Icons.settings,
                                                          color: Colors.white),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ),
                                          )
                                  ],
                                ),
                              );
                            }
                            if (state is LoadingWagonInfo) {
                              return const Padding(
                                padding: EdgeInsets.all(80),
                                child: CircularProgressIndicator(),
                              );
                            }
                            if (state is StationError) {
                              return Padding(
                                padding: const EdgeInsets.all(25),
                                child: Text(
                                  state.error,
                                  textAlign: TextAlign.center,
                                  style: const TextStyle(
                                    color: Color.fromRGBO(3, 69, 105, 1),
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20,
                                  ),
                                ),
                              );
                            }
                            if (state is WagonInformation) {
                              return Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  const Padding(
                                    padding: EdgeInsets.all(25),
                                    child: Text(
                                      'Descripcion encontrada: ',
                                      style: TextStyle(
                                        color: Color.fromRGBO(3, 69, 105, 1),
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20,
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(15),
                                    child: Text(
                                      description,
                                      textAlign: TextAlign.center,
                                      style: const TextStyle(
                                        color: Color.fromRGBO(3, 69, 105, 1),
                                        fontSize: 17,
                                      ),
                                    ),
                                  ),
                                ],
                              );
                            }
                            return Container();
                          },
                        );
                      }),
                    )),
              ],
            );
          },
        ),
      ),
    );
  }
}
